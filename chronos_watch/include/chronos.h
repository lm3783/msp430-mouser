// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************

#ifndef CHRONOS_H_
#define CHRONOS_H_

// *************************************************************************************************
// Chronos register map

// *************************************************************************************************
// Macro section
// Port and pin resource for SPI interface to acceleration sensor
// SDO=MOSI=P1.6, SDI=MISO=P1.5, SCK=P1.7
#define AS_SPI_IN            (P1IN)
#define AS_SPI_OUT           (P1OUT)
#define AS_SPI_DIR           (P1DIR)
#define AS_SPI_SEL           (P1SEL)
#define AS_SPI_REN           (P1REN)
#define AS_SDO_PIN           (BIT6)
#define AS_SDI_PIN           (BIT5)
#define AS_SCK_PIN           (BIT7)

// CSN=PJ.1
#define AS_CSN_OUT           (PJOUT)
#define AS_CSN_DIR           (PJDIR)
#define AS_CSN_PIN           (BIT1)

#define AS_TX_BUFFER         (UCA0TXBUF)
#define AS_RX_BUFFER         (UCA0RXBUF)
#define AS_TX_IFG            (UCTXIFG)
#define AS_RX_IFG            (UCRXIFG)
#define AS_IRQ_REG           (UCA0IFG)
#define AS_SPI_CTL0          (UCA0CTL0)
#define AS_SPI_CTL1          (UCA0CTL1)
#define AS_SPI_BR0           (UCA0BR0)
#define AS_SPI_BR1           (UCA0BR1)

// Port and pin resource for power-up of acceleration sensor, VDD=PJ.0
#define AS_PWR_OUT           (PJOUT)
#define AS_PWR_DIR           (PJDIR)
#define AS_PWR_PIN           (BIT0)

// Port, pin and interrupt resource for interrupt from acceleration sensor, CMA_INT=P2.5
#define AS_INT_IN            (P2IN)
#define AS_INT_OUT           (P2OUT)
#define AS_INT_DIR           (P2DIR)
#define AS_INT_IE            (P2IE)
#define AS_INT_IES           (P2IES)
#define AS_INT_IFG           (P2IFG)
#define AS_INT_PIN           (BIT5)

// *************************************************************************************************
// LCD Defines section
// LCD display modes
#define SEG_OFF                    (BIT0)
#define SEG_ON                                  (BIT1)
#define BLINK_OFF                 (BIT2)
#define BLINK_ON                 (BIT3)

// LCD line
#define LCD_L1                  (0)
#define LCD_L2                  (4)

// 7-segment character bit assignments
#define SEG_A                   (BIT4)
#define SEG_B                   (BIT5)
#define SEG_C                   (BIT6)
#define SEG_D                   (BIT7)
#define SEG_E                   (BIT2)
#define SEG_F                   (BIT0)
#define SEG_G                   (BIT1)

// ------------------------------------------
// LCD symbols for easier access
//
// xxx_SEG_xxx          = Seven-segment character (sequence 5-4-3-2-1-0)
// xxx_SYMB_xxx         = Display symbol, e.g. "AM" for ante meridiem
// xxx_UNIT_xxx         = Display unit, e.g. "km/h" for kilometers per hour
// xxx_ICON_xxx         = Display icon, e.g. heart to indicate reception of heart rate data
// xxx_L1_xxx           = Item is part of Line1 information
// xxx_L2_xxx           = Item is part of Line2 information

// LCD controller memory map
#define LCD_MEM_1                               ((u8*)0x0A20)
#define LCD_MEM_2                               ((u8*)0x0A21)
#define LCD_MEM_3                               ((u8*)0x0A22)
#define LCD_MEM_4                               ((u8*)0x0A23)
#define LCD_MEM_5                               ((u8*)0x0A24)
#define LCD_MEM_6                               ((u8*)0x0A25)
#define LCD_MEM_7                               ((u8*)0x0A26)
#define LCD_MEM_8                               ((u8*)0x0A27)
#define LCD_MEM_9                               ((u8*)0x0A28)
#define LCD_MEM_10                              ((u8*)0x0A29)
#define LCD_MEM_11                              ((u8*)0x0A2A)
#define LCD_MEM_12                              ((u8*)0x0A2B)
#define LCD_MEM_BLINK_OFFSET                    (0x20)

// Memory assignment
#define LCD_SEG_L1_0_MEM                        (LCD_MEM_6)
#define LCD_SEG_L1_1_MEM                        (LCD_MEM_4)
#define LCD_SEG_L1_2_MEM                        (LCD_MEM_3)
#define LCD_SEG_L1_3_MEM                        (LCD_MEM_2)
#define LCD_SEG_L1_COL_MEM                      (LCD_MEM_1)
#define LCD_SEG_L1_DP1_MEM                      (LCD_MEM_1)
#define LCD_SEG_L1_DP0_MEM                      (LCD_MEM_5)
#define LCD_SEG_L2_0_MEM                        (LCD_MEM_8)
#define LCD_SEG_L2_1_MEM                        (LCD_MEM_9)
#define LCD_SEG_L2_2_MEM                        (LCD_MEM_10)
#define LCD_SEG_L2_3_MEM                        (LCD_MEM_11)
#define LCD_SEG_L2_4_MEM                        (LCD_MEM_12)
#define LCD_SEG_L2_5_MEM                        (LCD_MEM_12)
#define LCD_SEG_L2_COL1_MEM                     (LCD_MEM_1)
#define LCD_SEG_L2_COL0_MEM                     (LCD_MEM_5)
#define LCD_SEG_L2_DP_MEM                       (LCD_MEM_9)
#define LCD_SYMB_AM_MEM                         (LCD_MEM_1)
#define LCD_SYMB_PM_MEM                         (LCD_MEM_1)
#define LCD_SYMB_ARROW_UP_MEM           (LCD_MEM_1)
#define LCD_SYMB_ARROW_DOWN_MEM         (LCD_MEM_1)
#define LCD_SYMB_PERCENT_MEM            (LCD_MEM_5)
#define LCD_SYMB_TOTAL_MEM                      (LCD_MEM_11)
#define LCD_SYMB_AVERAGE_MEM            (LCD_MEM_10)
#define LCD_SYMB_MAX_MEM                        (LCD_MEM_8)
#define LCD_SYMB_BATTERY_MEM            (LCD_MEM_7)
#define LCD_UNIT_L1_FT_MEM                      (LCD_MEM_5)
#define LCD_UNIT_L1_K_MEM                       (LCD_MEM_5)
#define LCD_UNIT_L1_M_MEM                       (LCD_MEM_7)
#define LCD_UNIT_L1_I_MEM                       (LCD_MEM_7)
#define LCD_UNIT_L1_PER_S_MEM           (LCD_MEM_5)
#define LCD_UNIT_L1_PER_H_MEM           (LCD_MEM_7)
#define LCD_UNIT_L1_DEGREE_MEM          (LCD_MEM_5)
#define LCD_UNIT_L2_KCAL_MEM            (LCD_MEM_7)
#define LCD_UNIT_L2_KM_MEM                      (LCD_MEM_7)
#define LCD_UNIT_L2_MI_MEM                      (LCD_MEM_7)
#define LCD_ICON_HEART_MEM                      (LCD_MEM_2)
#define LCD_ICON_STOPWATCH_MEM          (LCD_MEM_3)
#define LCD_ICON_RECORD_MEM                     (LCD_MEM_1)
#define LCD_ICON_ALARM_MEM                      (LCD_MEM_4)
#define LCD_ICON_BEEPER1_MEM            (LCD_MEM_5)
#define LCD_ICON_BEEPER2_MEM            (LCD_MEM_6)
#define LCD_ICON_BEEPER3_MEM            (LCD_MEM_7)

// Bit masks for write access
#define LCD_SEG_L1_0_MASK                       (BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_1_MASK                       (BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_2_MASK                       (BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_3_MASK                       (BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_COL_MASK                     (BIT5)
#define LCD_SEG_L1_DP1_MASK                     (BIT6)
#define LCD_SEG_L1_DP0_MASK                     (BIT2)
#define LCD_SEG_L2_0_MASK                       (BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_1_MASK                       (BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_2_MASK                       (BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_3_MASK                       (BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_4_MASK                       (BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_5_MASK                       (BIT7)
#define LCD_SEG_L2_COL1_MASK            (BIT4)
#define LCD_SEG_L2_COL0_MASK            (BIT0)
#define LCD_SEG_L2_DP_MASK                      (BIT7)
#define LCD_SYMB_AM_MASK                        (BIT1 + BIT0)
#define LCD_SYMB_PM_MASK                        (BIT0)
#define LCD_SYMB_ARROW_UP_MASK          (BIT2)
#define LCD_SYMB_ARROW_DOWN_MASK        (BIT3)
#define LCD_SYMB_PERCENT_MASK           (BIT4)
#define LCD_SYMB_TOTAL_MASK                     (BIT7)
#define LCD_SYMB_AVERAGE_MASK           (BIT7)
#define LCD_SYMB_MAX_MASK                       (BIT7)
#define LCD_SYMB_BATTERY_MASK           (BIT7)
#define LCD_UNIT_L1_FT_MASK                     (BIT5)
#define LCD_UNIT_L1_K_MASK                      (BIT6)
#define LCD_UNIT_L1_M_MASK                      (BIT1)
#define LCD_UNIT_L1_I_MASK                      (BIT0)
#define LCD_UNIT_L1_PER_S_MASK          (BIT7)
#define LCD_UNIT_L1_PER_H_MASK          (BIT2)
#define LCD_UNIT_L1_DEGREE_MASK         (BIT1)
#define LCD_UNIT_L2_KCAL_MASK           (BIT4)
#define LCD_UNIT_L2_KM_MASK                     (BIT5)
#define LCD_UNIT_L2_MI_MASK                     (BIT6)
#define LCD_ICON_HEART_MASK                     (BIT3)
#define LCD_ICON_STOPWATCH_MASK         (BIT3)
#define LCD_ICON_RECORD_MASK            (BIT7)
#define LCD_ICON_ALARM_MASK                     (BIT3)
#define LCD_ICON_BEEPER1_MASK           (BIT3)
#define LCD_ICON_BEEPER2_MASK           (BIT3)
#define LCD_ICON_BEEPER3_MASK           (BIT3)

// *************************************************************************************************
// Button Defines section
// Port, pins and interrupt resources for buttons
#define BUTTONS_IN              (P2IN)
#define BUTTONS_OUT                             (P2OUT)
#define BUTTONS_DIR             (P2DIR)
#define BUTTONS_REN                             (P2REN)
#define BUTTONS_IE              (P2IE)
#define BUTTONS_IES             (P2IES)
#define BUTTONS_IFG             (P2IFG)

// Button ports
#define BUTTON_STAR_PIN         (BIT2)
#define BUTTON_NUM_PIN          (BIT1)
#define BUTTON_UP_PIN           (BIT4)
#define BUTTON_DOWN_PIN         (BIT0)
#define BUTTON_BACKLIGHT_PIN    (BIT3)
#define ALL_BUTTONS                             (BUTTON_STAR_PIN + BUTTON_NUM_PIN + BUTTON_UP_PIN + \
                                                 BUTTON_DOWN_PIN + BUTTON_BACKLIGHT_PIN)
// Button IV Values
#define BUTTON_ESC_IV		(P2IV_P2IFG2)
#define BUTTON_ENTER_IV		(P2IV_P2IFG1)
#define BUTTON_UP_IV		(P2IV_P2IFG4)
#define BUTTON_DOWN_IV		(P2IV_P2IFG0)


// *************************************************************************************************
// I2C Defines section
// Port and pin resource for I2C interface to pressure sensor
// SCL=PJ.3, SDA=PJ.2, EOC=P2.6
#define PS_I2C_IN            (PJIN)
#define PS_I2C_OUT           (PJOUT)
#define PS_I2C_DIR           (PJDIR)
#define PS_I2C_REN           (PJREN)
#define PS_SCL_PIN           (BIT3)
#define PS_SDA_PIN           (BIT2)

// Port, pin and interrupt resource for interrupt from acceleration sensor, EOC=P2.6
#define PS_INT_IN            (P2IN)
#define PS_INT_OUT           (P2OUT)
#define PS_INT_DIR           (P2DIR)
#define PS_INT_IE            (P2IE)
#define PS_INT_IES           (P2IES)
#define PS_INT_IFG           (P2IFG)
#define PS_INT_PIN           (BIT6)

// I2C defines
#define PS_I2C_WRITE         (0u)
#define PS_I2C_READ          (1u)

#define PS_I2C_SEND_START    (0u)
#define PS_I2C_SEND_RESTART  (1u)
#define PS_I2C_SEND_STOP     (2u)
#define PS_I2C_CHECK_ACK     (3u)

#define PS_I2C_8BIT_ACCESS   (0u)
#define PS_I2C_16BIT_ACCESS  (1u)

#define PS_I2C_SCL_HI        { PS_I2C_OUT |=  PS_SCL_PIN; }
#define PS_I2C_SCL_LO        { PS_I2C_OUT &= ~PS_SCL_PIN; }
#define PS_I2C_SDA_HI        { PS_I2C_OUT |=  PS_SDA_PIN; }
#define PS_I2C_SDA_LO        { PS_I2C_OUT &= ~PS_SDA_PIN; }
#define PS_I2C_SDA_IN        { PS_I2C_OUT |=  PS_SDA_PIN; PS_I2C_DIR &= ~PS_SDA_PIN; }
#define PS_I2C_SDA_OUT       { PS_I2C_DIR |=  PS_SDA_PIN; }

// *************************************************************************************************
// Device Descriptor Table (TLV)
#define CAL_ADC_GAIN_FACTOR                          ((u16*)0x1A16)
#define CAL_ADC_OFFSET                               ((s16*)0x1A18)
#define CAL_ADC_V20_VREF_FACTOR						 ((u16*)0x1A2A)

// *************************************************************************************************
// Buzzer Defines section
#define BUZZER_OUT		(P2OUT)
#define BUZZER_SEL		(P2SEL)
#define BUZZER_PIN		(BIT7)
// Buzzer output signal frequency = 32,768kHz/(BUZZER_TIMER_STEPS+1)/2 = 1.5kHz
#define BUZZER_TIMER_STEPS                                      (10u)
// Buzzer on time. (1 / 1.5kHZ) * (BUZZER_ON_CYCLES / 2) = 16ms
#define BUZZER_ON_CYCLES                                         (50)

// =================================================================================================
// BMA250 acceleration sensor configuration
// =================================================================================================
// DCO frequency division factor determining speed of the acceleration sensor SPI interface
// Speed in Hz = 12MHz / AS_BR_DIVIDER (max. 10MHz)
#define BMP_AS_BR_DIVIDER  (2u)

// BMP085 standard pressure
#define BMP_STANDARD_PRESSURE 101325

#endif /* CHRONOS_H_ */
