#ifndef BMA250_H
#define BMA250_H

//BMA250 Register map
#define BMA250_CHIPID           (0x00)
#define BMA250_ACC_X_LSB        (0x02)
#define BMA250_ACC_X_MSB        (0x03)
#define BMA250_ACC_Y_LSB        (0x04)
#define BMA250_ACC_Y_MSB        (0x05)
#define BMA250_ACC_Z_LSB        (0x06)
#define BMA250_ACC_Z_MSB        (0x07)

#define BMA250_INTERRUPT_STATUS (0x09)
#define BMA250_FLAT_INT         (BIT7)
#define BMA250_ORIENT_INT         (BIT6)
#define BMA250_S_TAP_INT         (BIT5)
#define BMA250_D_TAP_INT         (BIT4)
#define BMA250_SLOPE_INT         (BIT2)
#define BMA250_HIGH_INT         (BIT1)
#define BMA250_LOW_INT         (BIT0)

#define BMA250_NEW_DATA_STATUS (0x0A)
#define BMA250_DATA_INT         (BIT7)

#define BMA250_G_RANGE           (0x0F)	   // g Range
#define BMA250_G_RANGE_2G           (0x03)
#define BMA250_G_RANGE_4G           (0x05)
#define BMA250_G_RANGE_8G           (0x08)
#define BMA250_G_RANGE_16G           (0x0C)

#define BMA250_BANDWIDTH              (0x10)	   // Bandwidth
#define BMA250_BANDWIDTH_8HZ              (0x08)
#define BMA250_BANDWIDTH_16HZ              (0x09)
#define BMA250_BANDWIDTH_31HZ              (0x0A)
#define BMA250_BANDWIDTH_62HZ              (0x0B)
#define BMA250_BANDWIDTH_125HZ              (0x0C)
#define BMA250_BANDWIDTH_250HZ              (0x0D)
#define BMA250_BANDWIDTH_500HZ              (0x0E)
#define BMA250_BANDWIDTH_1000HZ              (0x0F)

#define BMA250_POWERMODE               (0x11)	   // Power modes
#define BMA250_POWERMODE_500US               (0x4A)
#define BMA250_POWERMODE_1MS               (0x4C)
#define BMA250_POWERMODE_2MS               (0x4E)
#define BMA250_POWERMODE_4MS               (0x50)
#define BMA250_POWERMODE_6MS               (0x52)
#define BMA250_POWERMODE_10MS               (0x54)
#define BMA250_POWERMODE_25MS               (0x56)
#define BMA250_POWERMODE_50MS               (0x58)
#define BMA250_POWERMODE_100MS               (0x5A)
#define BMA250_POWERMODE_500MS               (0x5C)
#define BMA250_POWERMODE_1S               (0x5E)
#define BMA250_POWERMODE_SUSPEND            (BIT7)

#define BMA250_SCR              (0x13)	   // Special Control Register
#define BMA250_DATA_HIGH_BW     (BIT7)
#define BMA250_SHADOW_DIS       (BIT6)

#define BMA250_RESET            (0x14)	   // Soft reset register (writing 0xB6 causes reset)
#define BMA250_RESET_VALUE      (0xB6)

#define BMA250_ISR1             (0x16)	   // Interrupt settings register 1
#define BMA250_FLAT_EN          (BIT7)
#define BMA250_ORIENT_EN          (BIT6)
#define BMA250_S_TAP_EN          (BIT5)
#define BMA250_D_TAP_EN          (BIT4)
#define BMA250_SLOPE_EN_Z       (BIT2)
#define BMA250_SLOPE_EN_Y          (BIT1)
#define BMA250_SLOPE_EN_X          (BIT0)

#define BMA250_ISR2             (0x17)	   // Interrupt settings register 2
#define BMA250_DATA_EN          (BIT4)
#define BMA250_LOW_EN          (BIT3)
#define BMA250_HIGH_EN_Z          (BIT2)
#define BMA250_HIGH_EN_Y          (BIT1)
#define BMA250_HIGH_EN_X          (BIT0)

#define BMA250_IMR1             (0x19)	   // Interrupt mapping register 1
#define BMA250_FLAT_INT1         (BIT7)
#define BMA250_ORIENT_INT1         (BIT6)
#define BMA250_S_TAP_INT1         (BIT5)
#define BMA250_D_TAP_INT1         (BIT4)
#define BMA250_SLOPE_INT1         (BIT2)
#define BMA250_HIGH_INT1         (BIT1)
#define BMA250_LOW_INT1         (BIT0)

#define BMA250_IMR2             (0x1A)	   // Interrupt mapping register 2
#define BMA250_DATA_INT1         (BIT0)

#define BMA250_LATCH_INT         (0x21)
#define BMA250_INT_RESET        (BIT7)
#define BMA250_INT_NON_LATCHED     (0x00)
#define BMA250_INT_250MS         (0x01)
#define BMA250_INT_500MS         (0x02)
#define BMA250_INT_1S         (0x03)
#define BMA250_INT_2S         (0x04)
#define BMA250_INT_4S         (0x05)
#define BMA250_INT_8S         (0x06)
#define BMA250_INT_500US         (0x0A)
#define BMA250_INT_1MS         (0x0B)
#define BMA250_INT_12MS         (0x0C)
#define BMA250_INT_25MS         (0x0D)
#define BMA250_INT_50MS         (0x0E)
#define BMA250_INT_LATCHED         (0x0F)

#define BMA250_LOW_DUR         (0x22)
#define BMA250_LOW_TH         (0x23)
#define BMA250_LOW_MODE         (0x24)
#define BMA250_HIGH_DUR         (0x25)
#define BMA250_HIGH_TH         (0x26)
#define BMA250_SLOPE_DUR         (0x27)
#define BMA250_SLOPE_TH         (0x28)
#define BMA250_TAP_DUR         (0x2A)
#define BMA250_TAP_TH           (0x2B)
#define BMA250_ORIENT_MODE           (0x2C)
#define BMA250_ORIENT_THERA           (0x2D)
#define BMA250_FLAT_THERA           (0x2E)
#define BMA250_FLAT_HOLD_TIME           (0x2F)

#define BMA250_NVM_CONTROL      (0x33)
#define BMA250_NVM_LOAD     (BIT3)
#define BMA250_NVM_RDY     (BIT2)
#define BMA250_NVM_PROG_TRIG     (BIT1)
#define BMA250_NVM_PROG_MODE     (BIT0)

#define BMA250_OFFSET       (0x36)
#define BMA250_OFFSET_RESET (BIT7)
#define BMA250_CAL_TRIGGER_1    (BIT6)
#define BMA250_CAL_TRIGGER_0    (BIT5)
#define BMA250_CAL_RDY    (BIT4)
#define BMA250_HP_Z_EN    (BIT2)
#define BMA250_HP_Y_EN    (BIT1)
#define BMA250_HP_X_EN    (BIT0)

#define BMA250_OFFSET_TARGET       (0x37)
#define BMA250_CUT_OFF      (BIT0)

#define BMA250_OFFSET_FILT_X       (0x39)
#define BMA250_OFFSET_FILT_Y       (0x3A)
#define BMA250_OFFSET_FILT_Z       (0x3B)

#endif // BMA250_H
