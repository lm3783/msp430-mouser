/*
 * bmp085.h
 *
 *  Created on: 2014年8月20日
 *      Author: Code
 */

#ifndef BMP085_H_
#define BMP085_H_

// Bosch BMP085 defines

#define BMP_085_CHIP_ID	     (0x55)
#define BMP_085_I2C_ADDR     (0xEE)

#define BMP_085_PROM_START_ADDR (0xAA)
#define BMP_085_CHIP_ID_REG	 (0xD0)
#define BMP_085_VERSION_REG	 (0xD1)

#define BMP_085_CTRL_MEAS_REG (0xF4)
#define BMP_085_ADC_OUT_MSB_REG	(0xF6)
#define BMP_085_ADC_OUT_LSB_REG	(0xF7)
#define BMP_085_ADC_OUT_XLSB_REG	(0xF8)

#define BMP_085_SOFT_RESET_REG (0xE0)

#define BMP_085_T_MEASURE    (0x2E)				 // temperature measurent
#define BMP_085_P_MEASURE    (0x34)				 // pressure measurement
#define BMP_085_P_OSS_ULTRA_LOW_POWER    (0)	 //ultra low power mode
#define BMP_085_P_OSS_STANDARD    (1)			 //standard mode
#define BMP_085_P_OSS_HIGH_RESOLUTION    (2)	 //high resolution mode
#define BMP_085_P_OSS_ULTRA_HIGH_RESOLUTION    (3)	 //ultra high resolution mode

#define BMP_SMD500_PARAM_MG  (3038)              //calibration parameter
#define BMP_SMD500_PARAM_MH  (-7357)             //calibration parameter
#define BMP_SMD500_PARAM_MI  (3791)              //calibration parameter
#define BMP_SMD500_PARAM_MJ  (64385)             //calibration parameter

#endif /* BMP085_H_ */
