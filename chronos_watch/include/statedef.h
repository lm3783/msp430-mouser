// *************************************************************************************************
// Public header for all state definition.
// *************************************************************************************************

#ifndef STATEDEF_H_
#define STATEDEF_H_

// Radio state
#define RADIO_IDLE			1	// Idle state
#define RADIO_RX_WOR_TIMEOUT		4	// Receiving
#define RADIO_TX_TRANSMITTING	5	// Transmitting
#define RADIO_TX_FINISHED	6	// Transmit complete
#define RADIO_RX_WOR		7	// Receiving
#define RADIO_RX_RECEIVING	8	// Receiving
#define RADIO_RX_FINISHED	9	// Receive complete. Checking CRC

// Radio command
// *************************************************************************************************
// @fn          RADIO_OPERATION_NOP
// @brief       No operation. Test comm link.
// @param       none
// *************************************************************************************************
#define RADIO_OPERATION_NOP		0x00

// *************************************************************************************************
// @fn          RADIO_OPERATION_RSSI
// @brief       RSSI ping package.
// @param    1: package type. 0:single ping; 1:continuous ping(distance measure); 2:motion detection ping
//				3:ask ap to start continuous ping; 4:ask ap to stop continuous ping
// *************************************************************************************************
#define	RADIO_OPERATION_RSSI		0x01

// *************************************************************************************************
// @fn          RADIO_OPERATION_PPT
// @brief       keyboard simulation package.
// @param    1: keycode index. 0:reserved(reset); 1:up key; 2:down key; 3:single tap
// *************************************************************************************************
#define	RADIO_OPERATION_PPT		0x02

// *************************************************************************************************
// @fn          RADIO_OPERATION_BICOM
// @brief       bidirectional communication package.
// @param    1: type. 0:from watch to AP with ack; 1:from watch to AP with data; 2:from AP to watch with display text; 3:from AP to watch with data;
//		   2-x: ACK mode: serial number. Other mode: actual data.
// *************************************************************************************************
#define	RADIO_OPERATION_BICOM		0x03

#endif /* STATEDEF_H_ */
