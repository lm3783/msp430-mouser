/*
 * config.h
 *
 *  Created on: 2014年8月20日
 *      Author: Code
 */

#ifndef CONFIG_H_
#define CONFIG_H_

// *************************************************************************************************
// Configure section

// Comment this to not use the LCD charge pump
//#define USE_LCD_CHARGE_PUMP

// Backlight time  (sec)
#define BACKLIGHT_TIME_ON                       (2u)

// Keep alarm for 10 on-off cycles
#define ALARM_ON_DURATION       (10u)

// Default pressure measure mode
#define BMP_085_P_OSS BMP_085_P_OSS_HIGH_RESOLUTION

// Default acceleration result. 8 bit can simpify the calculation
// #define BMA_250_8BIT_RESULT

//debug mode (remove key debounce)
//#define DEBUG_MODE

#ifdef DEBUG_MODE
#define MENU_DEFAULT_ENTRY	pressure_active
#else
#define MENU_DEFAULT_ENTRY	clock_active
#endif

//Default time setting
#define DEFAULT_SEC     (0)
#define DEFAULT_MIN     (40)
#define DEFAULT_HOUR     (21)
#define DEFAULT_DOW     (4)
#define DEFAULT_DAY     (4)
#define DEFAULT_MONTH     (2)
#define DEFAULT_YEAR     (2022)
#define DEFAULT_AMIN     (30)
#define DEFAULT_AHOUR     (6)

#endif /* CONFIG_H_ */
