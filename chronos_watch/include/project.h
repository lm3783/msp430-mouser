// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************

#ifndef PROJECT_H_
#define PROJECT_H_

// *************************************************************************************************
// Include section
#include <msp430.h>
#include "bm.h"
#include "chronos.h"
#include "config.h"
#include "statedef.h"

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section
// Set of system flags
typedef union
{
    struct
    {
        u16 clock_sec : 1;             // Clock second changed
        u16 clock_min : 1;  		   // Clock minute changed
        u16 clock_day : 1;             // Clock day changed
        u16 clock_alarm : 1;           // Clock alarm reached
        u16 timer_alarm : 1;
        u16 as_ready : 1;                  //
        u16 ps_ready : 1;                  //
        u16 adc12_data_ready : 1;
        u16 ta_timeup : 1;					//TA timer timeup
    } flag;
    u16 all_flags;                        // Shortcut to all system flags (for reset)
} s_system_flags;

typedef union
{
    struct
    {
        u16 alarm_enabled : 1;             //
        u16 snooze_enabled : 1;            //
        u16 timer_enabled : 1;             //
        u16 stopwatch_enabled : 1;			//
        u16 logger_enabled : 1;              //
        u16 backlight_enabled : 1;         //
        u16 buzzer_enabled : 1;         //

    } flag;
    u16 all_flags;                        // Shortcut to all system flags (for reset)
} s_system_status;

typedef union
{
    struct
    {
        u16 clock_minute : 1;             //
        u16 clock_second : 1;            //
        u16 backlight : 1;             //
        u16 timer : 1;			//
        u16 buzzer : 1;              //
        u16 pressure : 1;         //
        u16 radio : 1;         //

    } flag;
    u16 all_flags;                        // Shortcut to all system flags (for reset)
} s_rtc_user;

typedef struct {			//timer preset
	u8 hour;
	u8 minute;
	u8 second;
} timer_preset_t;

extern volatile s_system_flags sysflags;	//system flags
extern volatile s_system_status sysstats;
extern timer_preset_t current_timer;	//timer counter
extern timer_preset_t stopwatch_timer;	//stopwarch timer counter
extern volatile s_rtc_user rtc_user;		//RTC user number
extern u16 key_status;	//key press status
extern u8 menu_page;	//menu page number
extern u8 current_day;	//current day
extern u16 adc12_result;  //voltage
extern u16 buzzer_cycle;	//timer cycle, used for buzzer set
extern volatile u8 radio_state;	//radio state machine

//Module List
extern void (*current_program)(void);
extern void process_global_event(void);
extern void menu_active(void);
extern void alarm_active(void);
extern void clock_active(void);
extern void timer_active(void);
extern void stopwatch_active(void);
extern void pressure_active(void);
extern void battery_active(void);
extern void acceleration_active(void);
extern void wireless_rssi_active(void);
extern void wireless_ppt_active(void);
extern void wireless_bicom_active(void);

#endif                                    /*PROJECT_H_ */
