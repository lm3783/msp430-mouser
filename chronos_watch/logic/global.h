/*
 * global.h
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

void mcu_init(void);
void process_global_event(void);

#endif /* GLOBAL_H_ */
