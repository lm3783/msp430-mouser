// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Temperature measurement functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"

// driver
#include "display.h"
#include "bmp_as.h"
// logic
#include "acceleration.h"

// *************************************************************************************************
// Global Variable section

// Conversion values from data to mgrav taken from BMA250 datasheet (rev 1.05, figure 4)
const u16 bmp_mgrav_per_bit[] = { 4, 8, 16, 31, 63, 125, 250, 500, 1000 };
// Conversion values from data to mgrav taken from CMA3000-D0x datasheet (rev 0.4, table 4)

// *************************************************************************************************
// Extern section


// *************************************************************************************************
// @fn          convert_acceleration_value_to_mgrav
// @brief       Converts measured value to mgrav units
// @param       u16 value        g data from sensor
//				u8 sign			 whether sign need to be converted
// @return      u16                     Acceleration (mgrav)
// *************************************************************************************************
u16 convert_acceleration_value_to_mgrav(u16 value, u8 sign)
{
    s16 result;
    s8 i;
#ifdef BMA_250_8BIT_RESULT
    if(value & BIT7) {
        // Convert 2's complement negative number to positive number
        value = ~value;
        value += 1;
    }
    result = 0;
    for (i = 6; i >= 0; i--)
    {
    	result += ((value & (BIT(i))) >> i) * bmp_mgrav_per_bit[i + 2];
    }

#else
    if(value & BIT9) {
        // Convert 2's complement negative number to positive number
        value = ~value;
        value += 1;
    }

    result = 0;
    for (i = 8; i >= 0; i--)
    {
    	result += ((value & (BIT(i))) >> i) * bmp_mgrav_per_bit[i];
    }
#endif
    return (result);
}

// *************************************************************************************************
// @fn          acceleration_active
// @brief       Get sensor data and run pedometer alg
// @param       none
// @return      none
// *************************************************************************************************
void acceleration_active(void)
{
    u16 as_data[3];
	// clear display
	display_chars(0, 10, "", SEG_OFF);
	display_values(0, 0, SEG_OFF | SEG_ON);	//accel
	// Start sensor
	bmp_as_start(BMP_AS_MODE_NORMAL);

    // loop stage
    u8 module_loop = 1;
    while(module_loop) {
    	// enable CC430 interrupt pin for data read out from acceleration sensor
    	AS_INT_IFG &= ~AS_INT_PIN;                   // Reset flag
    	AS_INT_IE  |=  AS_INT_PIN;                   // Enable interrupt
    	// When idle go to LPM3
        _BIS_SR(LPM3_bits + GIE);
        // Disable interrupt
    	AS_INT_IE &= ~AS_INT_PIN;                    // Disable interrupt

        //Global process
        process_global_event();

        if(sysflags.flag.as_ready) {
        	sysflags.flag.as_ready = 0;

        	bmp_as_get_data(as_data);
        }
        if(key_status == BUTTON_UP_IV) {
			//TODO
		} else if(key_status == BUTTON_DOWN_IV) {
			// TODO
		} else if(key_status == BUTTON_ESC_IV) {
        	//exit program
        	current_program = clock_active;
            module_loop = 0;
        }
        key_status = 0;
    }

    // Stop acceleration sensor
	bmp_as_stop();
}
