/*
 * battery.c
 *
 *  Created on: 2014年8月17日
 *      Author: Code
 */

// system
#include "project.h"

// driver
#include "display.h"
#include "adc12.h"

// logic
#include "battery.h"

// *************************************************************************************************
// @fn          battery_active
// @brief       Display battery voltage.
// @param       none
// @return      none
// *************************************************************************************************
void battery_active(void)
{
	//init

	//static display element
	write_lcd_mem(LCD_SEG_L2_DP_MEM, LCD_SEG_L2_DP_MASK, LCD_SEG_L2_DP_MASK, SEG_ON);  // .
	write_lcd_mem(LCD_SYMB_BATTERY_MEM, LCD_SYMB_BATTERY_MASK,LCD_SYMB_BATTERY_MASK, SEG_ON);  // voltage

	adc12_single_conversion();		//first measurement

    // loop stage
    u8 module_loop = 1;
    while(module_loop) {
        // When idle go to LPM3
        _BIS_SR(LPM3_bits + GIE);

        //Global process
        process_global_event();

        if(sysflags.flag.adc12_data_ready) {
        	sysflags.flag.adc12_data_ready = 0;

        	// Calibration
        	// https://e2e.ti.com/support/microcontrollers/msp-low-power-microcontrollers-group/msp430/f/msp-low-power-microcontroller-forum/204428/msp430-adc-calibration-using-tlv-correction-values-at-bella
        	// ADC(calibrated) = ( (ADC(raw) x CAL_VREF_FACTOR / 2^15) x (CAL_ADC_GAIN_FACTOR / 2^15) ) + CAL_ADC_OFFSET
        	u32 adc_result = ((u32)adc12_result * (*CAL_ADC_V20_VREF_FACTOR)) >> 15;
        	adc_result = (adc_result * (*CAL_ADC_GAIN_FACTOR)) >> 15;
        	adc12_result = adc_result + (*CAL_ADC_OFFSET);

        	// Convert ADC value to "x.xx V"
			// Ideally we have A11=0->AVCC=0V ... A11=4095(2^12-1)->AVCC=4V
			// --> (A11/4095)*4V=AVCC --> AVCC=(A11*4)/4095
			adc12_result = (adc12_result * 2 * 2) / 41;
        	display_values(4, adc12_result, SEG_OFF | SEG_ON);	//voltage
        }

        if(key_status == BUTTON_DOWN_IV) {
        	//tdo another measurement
        	adc12_single_conversion();
        } else if(key_status == BUTTON_ESC_IV) {
        	//exit program
        	current_program = clock_active;
            module_loop = 0;
        }
        key_status = 0;
    }

    //remove display element
	write_lcd_mem(LCD_SEG_L2_DP_MEM, LCD_SEG_L2_DP_MASK, LCD_SEG_L2_DP_MASK, SEG_OFF);  // .
	write_lcd_mem(LCD_SYMB_BATTERY_MEM, LCD_SYMB_BATTERY_MASK,LCD_SYMB_BATTERY_MASK, SEG_OFF);  // voltage
}
