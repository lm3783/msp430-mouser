// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Alarm routines.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"

// driver
#include "display.h"

// logic
#include "alarm.h"

// *************************************************************************************************
// Prototypes section

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section

// *************************************************************************************************
// Extern section

// *************************************************************************************************
// @fn          alarm_display_time
// @brief       display alarm time
// @param       none
// @return      none
// *************************************************************************************************
void alarm_display_time(void)
{
	display_alarm_value(2, RTCAHOUR, SEG_OFF | SEG_ON, 0);
	display_alarm_value(0, RTCAMIN, SEG_OFF | SEG_ON, 1);
	display_char(6, 'Z', SEG_OFF | SEG_ON);
	display_value(4, (int)sysstats.flag.snooze_enabled, SEG_OFF | SEG_ON, 0);
}

// *************************************************************************************************
// @fn          alarm_active
// @brief       Display alarm time.
// @param       none
// @return      none
// *************************************************************************************************
void alarm_active(void)
{
	//init stage
	// setup blink
	write_lcd_mem(LCD_SEG_L1_COL_MEM, LCD_SEG_L1_COL_MASK, LCD_SEG_L1_COL_MASK, SEG_ON);  // :
	write_lcd_mem(LCD_SEG_L2_COL0_MEM, LCD_SEG_L2_COL0_MASK, LCD_SEG_L2_COL0_MASK, SEG_ON);  // :
	alarm_display_time();	//display time and date

	u8 loop = 1;
	while(loop) {
		// When idle go to LPM3
		_BIS_SR(LPM3_bits + GIE);

		//key press
		if(key_status == BUTTON_UP_IV) {		//toggle alarm
			alarm_toggle();
		} else if(key_status == BUTTON_DOWN_IV) {		//toggle snooze
			sysstats.flag.snooze_enabled = !(sysstats.flag.snooze_enabled);
			alarm_display_time();
		} else if(key_status == BUTTON_ESC_IV) {		//return to clock mode
			current_program = clock_active;
			loop = 0;
		} else if(key_status == BUTTON_ENTER_IV) {	//change alarm
			RTCCTL01 &= ~RTCAIE;	//disable alarm
			alarm_setting();
			if(sysstats.flag.alarm_enabled) {
				RTCCTL01 |= RTCAIE;	//enable alarm
			}
			alarm_display_time();
		}
		key_status = 0;
	}
}

// *************************************************************************************************
// @fn          alarm_setting
// @brief       set alarm time
// @param       none
// @return      none
// *************************************************************************************************
void alarm_setting(void)
{
    u8 result;
    LCDBBLKCTL |= LCDBLKMOD0; // start blink
    result = change_value(2, RTCAHOUR, 0, 23, 1);	//hour
    if(result == 0xff) {
    	return;
    } else {
    	RTCAHOUR = result;
    }
    result = change_value(0, RTCAMIN, 0, 59, 1);		//minute
    if(result == 0xff) {
    	return;
    } else {
    	RTCAMIN = result;
    }
    LCDBBLKCTL &= ~LCDBLKMOD0; // stop blink
}

// *************************************************************************************************
// @fn          alarm_toggle
// @brief       toggle alarm
// @param       none
// @return      none
// *************************************************************************************************
void alarm_toggle(void)
{
	if(sysstats.flag.alarm_enabled) {
		//disable alarm
		sysstats.flag.alarm_enabled = 0;
		RTCCTL01 &= ~RTCAIE;
		write_lcd_mem(LCD_ICON_ALARM_MEM, LCD_ICON_ALARM_MASK, LCD_ICON_ALARM_MASK, SEG_OFF);
	} else {
		//enable alarm
		sysstats.flag.alarm_enabled = 1;
		RTCCTL01 |= RTCAIE;
		write_lcd_mem(LCD_ICON_ALARM_MEM, LCD_ICON_ALARM_MASK, LCD_ICON_ALARM_MASK, SEG_ON);
	}
}
