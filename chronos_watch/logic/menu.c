/*
 * menu.c
 *
 *  Created on: 2014骞�鏈�0鏃�
 *      Author: Code
 */

// system
#include <string.h>
#include "project.h"
#include "menu.h"

//driver
#include "display.h"

// *************************************************************************************************
// @fn          menu_init
// @brief       Menu routine.
// @param       none
// @return      none
// *************************************************************************************************
void menu_init(void)
{
	strcpy(menu_item[0].display, "AL@ST@@TM");	//TM(timer) ST(stopwatch) AL(alarm)
	menu_item[0].up_program = timer_active;
	menu_item[0].down_program = stopwatch_active;
	menu_item[0].enter_program = alarm_active;

	strcpy(menu_item[1].display, "VO@AC@@PR");	//PR(pressure) AC(acclometer) VO(voltage)
	menu_item[1].up_program = pressure_active;
	menu_item[1].down_program = acceleration_active;
	menu_item[1].enter_program = battery_active;

	strcpy(menu_item[2].display, "BI@RS@@PT");	//PT(Remote ppt control) RS(RSSI measurement) BI(bidirectional communication)
	menu_item[2].up_program = wireless_ppt_active;
	menu_item[2].down_program = wireless_rssi_active;
	menu_item[2].enter_program = wireless_bicom_active;

	strcpy(menu_item[3].display, "BS@--@@SY");	//SY(Sync mode) -- BS(RFBSL update)
	menu_item[3].up_program = pressure_active;
	menu_item[3].down_program = acceleration_active;
	menu_item[3].enter_program = battery_active;

}

// *************************************************************************************************
// @fn          menu_active
// @brief       Menu routine.
// @param       none
// @return      none
// *************************************************************************************************
void menu_active(void)
{
	if(menu_page < MENU_PAGE_NUMBER) {
		display_chars(0, 9, menu_item[menu_page].display,  SEG_OFF | SEG_ON);
	} else {
		menu_page = 0;
		current_program = clock_active;
		return;
	}

	// loop stage
	u8 module_loop = 1;
	while(module_loop) {
		// When idle go to LPM3
		_BIS_SR(LPM3_bits + GIE);

		if(key_status) {
			//key press
			//switch to program
			if(key_status == BUTTON_UP_IV) {
				current_program = menu_item[menu_page].up_program;
			} else if(key_status == BUTTON_DOWN_IV) {
				current_program = menu_item[menu_page].down_program;
			} else if(key_status == BUTTON_ESC_IV) {
				menu_page++;
			} else if(key_status == BUTTON_ENTER_IV) {
				current_program = menu_item[menu_page].enter_program;
			}
			key_status = 0;
			module_loop = 0;
		} else {
			//Global process
			//process_global_event();
		}
	}
}
