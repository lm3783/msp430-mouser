// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Time functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"
#include "clock.h"

// driver
#include "display.h"
#include "alarm.h"
#include "RTC.h"

// *************************************************************************************************
// @fn          clock_active
// @brief       Clock routine.
// @param       none
// @return      none
// *************************************************************************************************
void clock_active(void)
{
	//init stage
	// setup blink
	write_lcd_mem(LCD_SEG_L1_COL_MEM, LCD_SEG_L1_COL_MASK, LCD_SEG_L1_COL_MASK, SEG_ON | BLINK_ON);  // :
	write_lcd_mem(LCD_SEG_L2_DP_MEM, LCD_SEG_L2_DP_MASK, LCD_SEG_L2_DP_MASK, SEG_ON);  // .
	write_lcd_mem(LCD_SEG_L2_COL1_MEM, LCD_SEG_L2_COL1_MASK, LCD_SEG_L2_COL1_MASK, SEG_ON);  // :
	LCDBBLKCTL |= LCDBLKMOD0; // start blink

	RTCCTL01 &= ~RTCTEV__0000;	// switch to minute mode
	u8 display_mode = 1;	//date display mode
	clock_display_time();	//upper line
	clock_display_date(display_mode, 0);	//lower line


    // loop stage
    u8 module_loop = 1;
    while(module_loop) {
        // When idle go to LPM3
        _BIS_SR(LPM3_bits + GIE);

        //Global process
        process_global_event();

        //display update
        if((sysflags.flag.clock_sec && !display_mode) || (sysflags.flag.clock_day && display_mode)) {
        	clock_display_date(display_mode, 0);
        	sysflags.flag.clock_sec = 0;
        	sysflags.flag.clock_day = 0;
        } else if(sysflags.flag.clock_min) {
        	sysflags.flag.clock_min = 0;
        	clock_display_time();
        }

        //key press
        if(key_status == BUTTON_UP_IV) {
            alarm_toggle();
        } else if(key_status == BUTTON_DOWN_IV) {
            //change display style
        	display_mode = !display_mode;
            clock_display_date(display_mode, 1);
        } else if(key_status == BUTTON_ESC_IV) {
        	current_program = menu_active;
            module_loop = 0;
        } else if(key_status == BUTTON_ENTER_IV) {
        	//update display
        	if(display_mode == 0) {
        		display_mode = 1;
        		clock_display_date(display_mode, 1);
        	}
        	RTCCTL01 |= RTCHOLD;    //Stop the clock
        	clock_setting();	//set value
			RTCCTL01 &= ~RTCHOLD;	//start the clock
			LCDBMEMCTL |= LCDCLRBM;		//clear blinking memory
			write_lcd_mem(LCD_SEG_L1_COL_MEM, LCD_SEG_L1_COL_MASK, LCD_SEG_L1_COL_MASK, BLINK_ON);  // : blink
        }
        key_status = 0;
    }

    //clear stage
	LCDBBLKCTL &= ~LCDBLKMOD0; // stop blink
    RTCCTL01 |= RTCTEV__0000;	// switch to daily mode
    if(display_mode == 0) {
    	rtc_user.flag.clock_second = 0;	//disable second clock mode
    }
}

// *************************************************************************************************
// @fn          clock_display_time
// @brief       Display time(hh:mm)
// @param       none
// @return      none
// *************************************************************************************************
void clock_display_time(void)
{
    //hh:mm
    display_value(2, RTCHOUR, SEG_OFF | SEG_ON, 0);
    display_value(0, RTCMIN, SEG_OFF | SEG_ON, 1);
}

// *************************************************************************************************
// @fn          clock_display_date
// @brief       Display date or seconds
// @param       u8 mode. 1 for month and day, 0 for year and second.
//				u8 init		if initialize is needed
// @return      none
// *************************************************************************************************
void clock_display_date(u8 mode, u8 init)
{
	if(mode) {
        //MM:DD:YY
        display_value(8, RTCMON, SEG_OFF | SEG_ON, 0);
        display_value(6, RTCDAY, SEG_OFF | SEG_ON, 1);
        display_value(4, RTCYEAR - 2000, SEG_OFF | SEG_ON, 0);
        if(init) {
        	rtc_user.flag.clock_second = 0;	//disable clock mode
        }
    } else {
        //WW:__:SS
    	display_value(4, RTCSEC, SEG_OFF | SEG_ON, 1);
		display_value(8, RTCDOW, SEG_OFF | SEG_ON, 0);
		display_chars(6, 2, "", SEG_OFF);
    	if(init) {
	        RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
	        rtc_user.flag.clock_second = 1;	//enable clock mode
    	}
    }
}

// *************************************************************************************************
// @fn          clock_setting
// @brief       set time
// @param       none
// @return      none
// *************************************************************************************************
void clock_setting(void)
{
    u8 result;
    result = change_value(2, RTCHOUR, 0, 23, 0);	//hour
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCHOUR(result);
    }
    result = change_value(0, RTCMIN, 0, 59, 0);		//minute
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCMIN(result);
    }
    result = change_value(8, RTCMON, 1, 12, 0);		//month
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCMON(result);
    }
    result = change_value(6, RTCDAY, 1, 31, 0);		//day
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCDAY(result);
    }
    u16 year = RTCYEAR - 2000;
    result = change_value(4, year, 20, 40, 0);		//year
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCYEAR(result + 2000);
    }
    clock_display_date(0, 0);			// switch to second mode
    result = change_value(8, RTCDOW, 0, 6, 0);		//day of week
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCDOW(result);
    }
    result = change_value(4, RTCSEC, 0, 59, 0);		//second
    if(result == 0xff) {
    	return;
    } else {
    	SetRTCSEC(result);
    }
    clock_display_date(1, 0);			// switch to day mode
}
