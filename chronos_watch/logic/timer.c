/*
 * timer.c
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

// system
#include "project.h"

// driver
#include "display.h"

// logic
#include "timer.h"

// *************************************************************************************************
// @fn          timer_init
// @brief       Initialize timer.
// @param       none
// @return      none
// *************************************************************************************************
void timer_init(void)
{
	timer_preset[0].hour = 0;
	timer_preset[0].minute = 0;
	timer_preset[0].second = 0;

	timer_preset[1].hour = 0;
	timer_preset[1].minute = 5;
	timer_preset[1].second = 0;

	timer_preset[2].hour = 0;
	timer_preset[2].minute = 15;
	timer_preset[2].second = 0;

	timer_preset[3].hour = 0;
	timer_preset[3].minute = 30;
	timer_preset[3].second = 0;
}

// *************************************************************************************************
// @fn          timer_active
// @brief       Display timer.
// @param       none
// @return      none
// *************************************************************************************************
void timer_active(void)
{
	timer_init();
	u8 current_preset = 0;

	//static display element
	display_char(2, 'P', SEG_ON);
	write_lcd_mem(LCD_SEG_L1_COL_MEM, LCD_SEG_L1_COL_MASK, LCD_SEG_L1_COL_MASK, SEG_ON);  // :
	write_lcd_mem(LCD_SEG_L2_COL1_MEM, LCD_SEG_L2_COL1_MASK, LCD_SEG_L2_COL1_MASK, SEG_ON);  // :
	write_lcd_mem(LCD_SEG_L2_COL0_MEM, LCD_SEG_L2_COL0_MASK, LCD_SEG_L2_COL0_MASK, SEG_ON);  // :

	if(sysstats.flag.timer_enabled) {	//if the timer is already enabled, enable screen refresh
		RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
		rtc_user.flag.timer = 1;	//enable clock mode
	}
	timer_display_preset();

    // loop stage
    u8 module_loop = 1;
    while(module_loop) {
        // When idle go to LPM3
        _BIS_SR(LPM3_bits + GIE);

        //Global process
        process_global_event();

        if(sysflags.flag.clock_sec && sysstats.flag.timer_enabled) {
        	sysflags.flag.clock_sec = 0;
        	timer_display_preset();
        }

        if(key_status == BUTTON_UP_IV) {
        	//change preset
        	current_preset++;
        	if(current_preset == TIMER_PRESET_NUMBER) {
        		current_preset = 0;
        	}
        	current_timer = timer_preset[current_preset];
        	display_value(0, current_preset, SEG_OFF | SEG_ON, 0);	//preset number
        	timer_display_preset();
        } else if(key_status == BUTTON_DOWN_IV) {
        	//check setting
        	if(current_timer.hour + current_timer.minute + current_timer.second != 0) {
        		//start/stop timer
        		if(!sysstats.flag.timer_enabled) {
        			sysstats.flag.timer_enabled = 1;	//start timer
        			write_lcd_mem(LCD_ICON_STOPWATCH_MEM, LCD_ICON_STOPWATCH_MASK, LCD_ICON_STOPWATCH_MASK, SEG_ON);	//draw icon
        			RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
        			rtc_user.flag.timer = 1;	//this is to enable screen refresh
        		} else {
        			sysstats.flag.timer_enabled = 0;	//stop timer
        			write_lcd_mem(LCD_ICON_STOPWATCH_MEM, LCD_ICON_STOPWATCH_MASK, LCD_ICON_STOPWATCH_MASK, SEG_OFF);	//clear icon
        			rtc_user.flag.timer = 0;		//this is to disable screen refresh
        		}
        	}
        } else if(key_status == BUTTON_ESC_IV) {
        	//exit program
        	current_program = clock_active;
            module_loop = 0;
        } else if(key_status == BUTTON_ENTER_IV) {
        	if(!sysstats.flag.timer_enabled) {
        		//change value
        		timer_setting();	//set value
        	}
        }
        key_status = 0;
    }

    if(!(sysstats.flag.timer_enabled)) {	//if the timer is not running, disable timer active
    	rtc_user.flag.timer = 0;
   	}
}

// *************************************************************************************************
// @fn          timer_display_preset
// @brief       Display timer preset.
// @param       none
// @return      none
// *************************************************************************************************
void timer_display_preset(void)
{
	display_value(4, current_timer.second, SEG_OFF | SEG_ON, 1);	//second
	display_value(6, current_timer.minute, SEG_OFF | SEG_ON, 1);
	display_value(8, current_timer.hour, SEG_OFF | SEG_ON, 0);
}

// *************************************************************************************************
// @fn          timer_setting
// @brief       set timer time
// @param       none
// @return      none
// *************************************************************************************************
void timer_setting(void)
{
    u8 result;
    LCDBBLKCTL |= LCDBLKMOD0; // start blink

    result = change_value(8, current_timer.hour, 0, 19, 0);	//hour
    if(result == 0xff) {
    	return;
    } else {
    	current_timer.hour = result;
    }
    result = change_value(6, current_timer.minute, 0, 59, 0);	//minute
	if(result == 0xff) {
		return;
	} else {
		current_timer.minute = result;
	}
    result = change_value(4, current_timer.second, 0, 59, 0);	//second
	if(result == 0xff) {
		return;
	} else {
		current_timer.second = result;
	}
    LCDBBLKCTL &= ~LCDBLKMOD0; // stop blink
}

// *************************************************************************************************
// @fn          timer_set_snooze
// @brief       Set a 5-minute snooze timer.
// @param       none
// @return      none
// *************************************************************************************************
void timer_set_snooze(void)
{
	if(current_program == timer_active) {
		return;		//do not set timer in timer function
	}
	current_timer = timer_preset[1];	//5 min preset

	sysstats.flag.timer_enabled = 1;	//start timer
	write_lcd_mem(LCD_ICON_STOPWATCH_MEM, LCD_ICON_STOPWATCH_MASK, LCD_ICON_STOPWATCH_MASK, SEG_ON);	//draw icon
	RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
	rtc_user.flag.timer = 1;	//this is to enable timer count
}
