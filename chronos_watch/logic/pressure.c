/*
 * pressure.c
 *
 *  Created on: 2014年8月17日
 *      Author: Code
 */

// system
#include "project.h"

// driver
#include "display.h"
#include "bmp_ps.h"

// logic
#include "pressure.h"

// *************************************************************************************************
// @fn          pressure_active
// @brief       Display pressure and temperature.
// @param       none
// @return      none
// *************************************************************************************************
void pressure_active(void)
{
	bmp_085_result_t result;

	u8 display_mode = 1;	//0:pressure  1:attitude

	//static display element
	write_lcd_mem(LCD_SEG_L1_DP0_MEM, LCD_SEG_L1_DP0_MASK, LCD_SEG_L1_DP0_MASK, SEG_ON);  // .
	write_lcd_mem(LCD_SEG_L2_DP_MEM, LCD_SEG_L2_DP_MASK, LCD_SEG_L2_DP_MASK, SEG_ON);  // .
	write_lcd_mem(LCD_UNIT_L1_DEGREE_MEM, LCD_UNIT_L1_DEGREE_MASK,LCD_UNIT_L1_DEGREE_MASK, SEG_ON);  // degree

	RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
	rtc_user.flag.pressure = 1;	//enable clock mode

    // loop stage
    u8 module_loop = 1;
    while(module_loop) {
        // When idle go to LPM3
        _BIS_SR(LPM3_bits + GIE);

        //Global process
        process_global_event();

        if(sysflags.flag.clock_sec) {
        	sysflags.flag.clock_sec = 0;
        	bmp_ps_measure(&result);
        	display_values(0, result.temperature, SEG_OFF | SEG_ON);	//temperature
        	if(!display_mode) {
        		display_values(4, result.pressure, SEG_OFF | SEG_ON);	//pressure
        	} else {
        		s32 altitude = 8 * (BMP_STANDARD_PRESSURE - result.pressure);
        		//s32 altitude = conv_pa_to_centimeter(delta_p);
        		display_values(4, altitude, SEG_OFF | SEG_ON);	//altitude
        	}
        }

        if(key_status == BUTTON_DOWN_IV) {
        	//change display mode
        	if(display_mode) {
        		display_mode = 0;
        		write_lcd_mem(LCD_UNIT_L2_MI_MEM, LCD_UNIT_L2_MI_MASK,LCD_UNIT_L2_MI_MASK, SEG_OFF);  // mi
        	} else {
        		display_mode = 1;
        		write_lcd_mem(LCD_UNIT_L2_MI_MEM, LCD_UNIT_L2_MI_MASK,LCD_UNIT_L2_MI_MASK, SEG_ON);  // mi
        	}
        	//clear display
        	display_chars(6, 4, "", SEG_OFF);
        } else if(key_status == BUTTON_ESC_IV) {
        	//exit program
        	current_program = clock_active;
            module_loop = 0;
        } else if(key_status == BUTTON_ENTER_IV) {
        	//TODO enter calibrate value
        }
        key_status = 0;
    }

    rtc_user.flag.pressure = 0;	//remove rtc usage

    //remove display element
	write_lcd_mem(LCD_UNIT_L2_MI_MEM, LCD_UNIT_L2_MI_MASK,LCD_UNIT_L2_MI_MASK, SEG_OFF);  // mi

}
