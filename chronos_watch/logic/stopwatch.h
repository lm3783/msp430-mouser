/*
 * stopwatch.h
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

#ifndef STOPWATCH_H_
#define STOPWATCH_H_


void stopwatch_active(void);
void stopwatch_display(void);

#endif /* STOPWATCH_H_ */
