/*
 * global.c
 *
 *  Created on: 2014骞�鏈�0鏃�
 *      Author: Code
 */

// system
#include "project.h"

// driver
#include "global.h"
#include "display.h"
#include "bmp_as.h"
#include "bmp_ps.h"
#include "buzzer.h"
#include "timer.h"
#include "pmm.h"
#include "rf1a.h"
#include "RTC.h"

void process_global_event(void)
{
	if(sysflags.flag.clock_day) {
		current_day = RTCDAY;
		//get battery voltage
	}

	if(sysflags.flag.clock_alarm) {
		sysflags.flag.clock_alarm = 0;
		sysstats.flag.buzzer_enabled = 1;
		//sound the alarm
		start_buzzer();

		if(!sysstats.flag.timer_enabled && sysstats.flag.snooze_enabled) {
			//set a 5 min timer
			timer_set_snooze();
		}
	}

	if(sysflags.flag.timer_alarm) {
		sysflags.flag.timer_alarm = 0;
		write_lcd_mem(LCD_ICON_STOPWATCH_MEM, LCD_ICON_STOPWATCH_MASK, LCD_ICON_STOPWATCH_MASK, SEG_OFF);	//clear icon

		sysstats.flag.buzzer_enabled = 1;
		//sound the alarm
		start_buzzer();
	}
}

void mcu_init(void)
{
	// ---------------------------------------------------------------------
    // Configure PMM
    SetVCore(1);
    // Open PMM registers for write access
    PMMCTL0_H = 0xA5;
    // Set global high power request enable, required by radio module
    PMMCTL0_L |= PMMHPMRE;
    // Lock PMM registers for write access
    PMMCTL0_H = 0x00;

    // ---------------------------------------------------------------------
    // Enable 32kHz ACLK
    P5SEL |= 0x03;              // Select XIN, XOUT on P5.0 and P5.1
    UCSCTL6 &= ~XT1OFF;         // XT1 On, Highest drive strength
    UCSCTL6 |= XCAP_3;          // Internal load cap
    UCSCTL3 = SELA__XT1CLK;     // Select XT1 as FLL reference
    UCSCTL4 = SELA__XT1CLK | SELS__DCOCLKDIV | SELM__DCOCLKDIV;

    // ---------------------------------------------------------------------
    // Configure CPU clock for 12MHz
    _BIS_SR(SCG0);              // Disable the FLL control loop
    UCSCTL0 = 0x0000;           // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;        // Select suitable range
    UCSCTL2 = FLLD_1 + 0x16E;   // Set DCO Multiplier
    _BIC_SR(SCG0);              // Enable the FLL control loop

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 12 MHz / 32,768 Hz = 375000 = MCLK cycles for DCO to settle
    //__delay_cycles(375000);

    // Loop until XT1 & DCO stabilizes, use do-while to insure that
    // body is executed at least once
    do
    {
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
        SFRIFG1 &= ~OFIFG;      // Clear fault flags
    }
    while ((SFRIFG1 & OFIFG));

    // ---------------------------------------------------------------------
    // Configure port mapping
    volatile u8 *ptr;
    // Disable all interrupts
    __disable_interrupt();
    // Get write-access to port mapping registers:
    PMAPPWD = 0x02D52;
    // Allow reconfiguration during runtime:
    PMAPCTL = PMAPRECFG;
    // P2.7 = TA0CCR1A or TA1CCR0A output (buzzer output)
    ptr = &P2MAP0;
    *(ptr + 7) = PM_TA1CCR0A;
    P2OUT &= ~BIT7;
    P2DIR |= BIT7;
    // P1.5 = SPI MISO input
    ptr = &P1MAP0;
    *(ptr + 5) = PM_UCA0SOMI;
    // P1.6 = SPI MOSI output
    *(ptr + 6) = PM_UCA0SIMO;
    // P1.7 = SPI CLK output
    *(ptr + 7) = PM_UCA0CLK;
    // Disable write-access to port mapping registers:
    PMAPPWD = 0;
    // Re-enable all interrupts
    __enable_interrupt();

    // ---------------------------------------------------------------------
    // Init acceleration sensor
    // Deactivate connection to acceleration sensor
    bmp_as_stop();
	// Initialize SPI interface to acceleration sensor
	AS_SPI_CTL0 |= UCSYNC | UCMST | UCMSB        // SPI master, 8 data bits,  MSB first,
	               | UCCKPH;                     //  clock idle low, data output on falling edge
	AS_SPI_CTL1 |= UCSSEL1;                      // SMCLK as clock source
	AS_SPI_BR0   = BMP_AS_BR_DIVIDER;            // Low byte of division factor for baud rate
	AS_SPI_BR1   = 0x00;                         // High byte of division factor for baud rate
	AS_SPI_CTL1 &= ~UCSWRST;                     // Start SPI hardware

    // ---------------------------------------------------------------------
    // Init LCD
	// Clear entire display memory
	LCDBMEMCTL |= LCDCLRBM + LCDCLRM;
	// LCD_FREQ = ACLK/16/8 = 256Hz
	// Frame frequency = 256Hz/4 = 64Hz, LCD mux 4, LCD on
	LCDBCTL0 = (LCDDIV0 + LCDDIV1 + LCDDIV2 + LCDDIV3) | (LCDPRE0 + LCDPRE1) | LCD4MUX | LCDON;
	// LCB_BLK_FREQ = ACLK/8/4096 = 1Hz
	LCDBBLKCTL = LCDBLKPRE0 | LCDBLKPRE1 | LCDBLKDIV0 | LCDBLKDIV1 | LCDBLKDIV2 | LCDBLKMOD0;
	// I/O to COM outputs
	P5SEL |= (BIT5 | BIT6 | BIT7);
	P5DIR |= (BIT5 | BIT6 | BIT7);
	// Activate LCD output
	LCDBPCTL0 = 0xFFFF;         // Select LCD segments S0-S15
	LCDBPCTL1 = 0x00FF;         // Select LCD segments S16-S22
#ifdef USE_LCD_CHARGE_PUMP
	// Charge pump voltage generated internally, internal bias (V2-V4) generation
	LCDBVCTL = LCDCPEN | VLCD_2_72;
#endif


	// ---------------------------------------------------------------------
    // Init buttons
	// Set button ports to input
	BUTTONS_DIR &= ~ALL_BUTTONS;
	// Enable internal pull-downs
	BUTTONS_OUT &= ~ALL_BUTTONS;
	BUTTONS_REN |= ALL_BUTTONS;
	// IRQ triggers on rising edge
	BUTTONS_IES &= ~ALL_BUTTONS;
	// Reset IRQ flags
	BUTTONS_IFG &= ~ALL_BUTTONS;
	// Enable button interrupts
	BUTTONS_IE |= ALL_BUTTONS;

    // ---------------------------------------------------------------------
    // Configure RTC_A for use by the clock
    RTCCTL01 |= RTCMODE;     //Switch to Calendar mode
    // Use special retine to set RTC clock. See errata.
    SetRTCSEC(DEFAULT_SEC);   //Set default value
    SetRTCMIN(DEFAULT_MIN);
    SetRTCHOUR(DEFAULT_HOUR);
    SetRTCDOW(DEFAULT_DOW);
    SetRTCDAY(DEFAULT_DAY);
    SetRTCMON(DEFAULT_MONTH);
    SetRTCYEAR(DEFAULT_YEAR);
    RTCCTL01 &= ~RTCHOLD;    //Start the clock
    RTCCTL01 |= RTCTEV__0000;	// switch to daily mode
    RTCCTL01 |= RTCTEVIE;	// enable rtc interrupt
    RTCAHOUR = DEFAULT_AHOUR | BIT7;
    RTCAMIN = DEFAULT_AMIN | BIT7;

    // ---------------------------------------------------------------------
    // Configure Timer0 for use by the clock and delay functions
    // Set interrupt frequency to 1Hz
    TA0CCR0 = 32768 - 1;
    // Enable timer interrupt
    TA0CTL |=  TAIE;

    // ---------------------------------------------------------------------
    // Init pressure sensor
	PS_INT_DIR &= ~PS_INT_PIN;           // EOC is input
	PS_INT_IES &= ~PS_INT_PIN;           // Interrupt on EOC rising edge
	PS_I2C_OUT |= PS_SCL_PIN + PS_SDA_PIN; // SCL and SDA are high by default
	PS_I2C_DIR |= PS_SCL_PIN + PS_SDA_PIN; // SCL and SDA are outputs by default

    // Read calibration values
	bmp_ps_get_cal_param();

}
