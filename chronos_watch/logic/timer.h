/*
 * timer.h
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

#ifndef TIMER_H_
#define TIMER_H_

#define TIMER_PRESET_NUMBER   4

static timer_preset_t timer_preset[TIMER_PRESET_NUMBER];

void timer_init(void);
void timer_active(void);
void timer_display_preset(void);
void timer_setting(void);
void timer_set_snooze(void);

#endif /* TIMER_H_ */
