/*
 * wireless_ppt.c
 *
 *  Created on: 2014��12��16��
 *      Author: limiao
 */


#include "project.h"
#include "wireless_ppt.h"

//driver
#include "display.h"
#include "radio.h"
#include "pmm.h"
#include "bmp_as.h"

// *************************************************************************************************
// @fn          wireless_ppt_active
// @brief       main routine.
// @param       none
// @return      none
// *************************************************************************************************
void wireless_ppt_active(void)
{
	display_chars(0, 9, "CL@DN@@UP", SEG_ON);	// up, down, clear

	// ---------------------------------------------------------------------
    // Reset radio core
	// Set Vcore to 2 , which is needed by radio core all the time. See errata.
    SetVCore(2);
    radio_init();

    //init bmp sensor
    bmp_as_start(BMP_AS_MODE_TAP);

    u8 tx_mode;
	// loop stage
    u8 module_loop = 1;
    while(module_loop) {

        //Global process
        process_global_event();

    	//clear radio state
		if(radio_state == RADIO_TX_FINISHED) {
			radio_state = RADIO_IDLE;
			radio_setidle(0);
		}
		if(radio_state == RADIO_IDLE) {
	        // When idle go to LPM3
	   		_BIS_SR(LPM3_bits + GIE);
		}

		//process key event
        if(key_status == BUTTON_UP_IV) {
          	rf1a_txbuffer[0] = 2;
           	rf1a_txbuffer[1] = RADIO_OPERATION_PPT;
           	rf1a_txbuffer[2] = 1;
           	tx_mode = 1;
        } else if(key_status == BUTTON_DOWN_IV) {
          	rf1a_txbuffer[0] = 2;
           	rf1a_txbuffer[1] = RADIO_OPERATION_PPT;
           	rf1a_txbuffer[2] = 2;
           	tx_mode = 1;
        } else if(key_status == BUTTON_ENTER_IV) {
          	rf1a_txbuffer[0] = 2;
           	rf1a_txbuffer[1] = RADIO_OPERATION_PPT;
           	rf1a_txbuffer[2] = 0;
        } else if(key_status == BUTTON_ESC_IV) {
 				//exit program
				current_program = clock_active;
				module_loop = 0;
        }
        key_status = 0;

        //process as event
        if(sysflags.flag.as_ready) {
        	sysflags.flag.as_ready = 0;
          	rf1a_txbuffer[0] = 2;
           	rf1a_txbuffer[1] = RADIO_OPERATION_PPT;
           	rf1a_txbuffer[2] = 3;
           	tx_mode = 1;
        }

        //process tx
        if(tx_mode) {
        	radio_settx();
        	tx_mode = 0;
        }
    }

    // Cleanup
	// Reset Vcore, which will also disable radio core. See errata.
    SetVCore(1);
    // disable sensor
    bmp_as_stop();
}
