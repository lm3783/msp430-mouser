/*
 * wireless_ppt.c
 *
 *  Created on: 2014��12��16��
 *      Author: limiao
 */


#include "project.h"
#include "wireless_bicom.h"

//driver
#include "display.h"
#include "radio.h"
#include "pmm.h"
#include "statedef.h"

// *************************************************************************************************
// @fn          wireless_bicom_active
// @brief       main routine.
// @param       none
// @return      none
// *************************************************************************************************
void wireless_bicom_active(void)
{
    // ---------------------------------------------------------------------
    // Reset radio core
	// Set Vcore to 2 , which is needed by radio core all the time. See errata.
    SetVCore(2);
    radio_init();

    u8 tx_mode = 0;
    u8 rx_mode = 0;
    u8 rx_ack = 0;
    u8 tx_data = 0;		//data to be sent
    display_value(0, tx_data, SEG_OFF | SEG_ON, 0);

    // loop stage
    u8 module_loop = 1;
    while(module_loop) {

        //Global process
        process_global_event();

    	//clear radio state
        if(radio_state == RADIO_TX_FINISHED) {
        	radio_state = RADIO_IDLE;
        	radio_setidle(0);
        } else if(radio_state == RADIO_RX_FINISHED) {
			radio_state = RADIO_IDLE;
			u8 ret = radio_getrx();	//get rx data
			if(ret) {
				u8 length = rf1a_rxbuffer[0];
				write_lcd_mem(LCD_ICON_BEEPER3_MEM, LCD_ICON_BEEPER3_MASK, LCD_ICON_BEEPER3_MASK, SEG_ON);  // wireless symbol
				if(rf1a_rxbuffer[1] == 2) {		//from AP to watch with display text
					display_chars(4, length, (char*)(rf1a_rxbuffer + 2), SEG_ON | SEG_OFF);
				}
				rx_ack = 1;		//radio will be in idle mode, then to wor mode. Since ack flag is set, it will change to send ack.
			}
		} else if(radio_state == RADIO_RX_WOR) {
			//radio wake up.
			if(rx_ack) {	//send ack. end wor
				rx_ack = 0;
				rx_mode = 0;	//clear WOR flag
				write_lcd_mem(LCD_ICON_BEEPER3_MEM, LCD_ICON_BEEPER3_MASK, LCD_ICON_BEEPER3_MASK, SEG_OFF);  // clear wireless symbol
	        	rf1a_txbuffer[0] = 2;
	           	rf1a_txbuffer[1] = RADIO_OPERATION_BICOM;
	           	rf1a_txbuffer[2] = 0;		//ack package
	           	tx_mode = 1;	//active tx mode. This will also quit rx mode.
			} else {		//default wor rx
				radio_setrx();
			}
		} else if(radio_state == RADIO_RX_WOR_TIMEOUT) {
			radio_setrxwor(0);
		}

        //init rx mode only when radio is idle. So it won't affect tx mode.
		if(radio_state == RADIO_IDLE) {
			if(rx_mode) {
				radio_setrxwor(1);
	        } else {
				// When idle go to LPM3
		   		_BIS_SR(LPM3_bits + GIE);
	        }
		}

		//process key event
        if(key_status == BUTTON_UP_IV) {
        	//change tx data value
            u8 result;
            LCDBBLKCTL |= LCDBLKMOD0; // start blink
            result = change_value(0, tx_data, 0, 10, 0);	//data
            if(result == 0xff) {
            	return;
            } else {
            	tx_data = result;
            }
            LCDBBLKCTL &= ~LCDBLKMOD0; // stop blink
        } else if(key_status == BUTTON_ENTER_IV) {
          	//send data
        	rf1a_txbuffer[0] = 3;
           	rf1a_txbuffer[1] = RADIO_OPERATION_BICOM;
           	rf1a_txbuffer[2] = 1;	//data transfer
           	rf1a_txbuffer[3] = tx_data;
           	tx_mode = 1;
        } else if(key_status == BUTTON_DOWN_IV) {
        	//toggle rx
        	if(rx_mode) {
        		rx_mode = 0;
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_OFF);  // wireless symbol
        	} else {
        		rx_mode = 1;
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_ON);  // wireless symbol
        		radio_setidle(2);
        	}
        } else if(key_status == BUTTON_ESC_IV) {
        	if(rx_mode) {
        		rx_mode = 0;
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_OFF);  // wireless symbol
        		radio_setidle(2);
        	} else {
 				//exit program
				current_program = clock_active;
				module_loop = 0;
        	}
        }
        key_status = 0;

        //process tx
        if(tx_mode) {
        	if(rx_mode) {
        		radio_setidle(2);
        	}
        	radio_settx();
        	tx_mode = 0;
        }
    }

    // Clearup
	// Reset Vcore, which will also disable radio core. See errata.
    SetVCore(1);
}
