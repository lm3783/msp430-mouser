/*
 * menu.h
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

#ifndef MENU_H_
#define MENU_H_

#define MENU_PAGE_NUMBER   4

typedef struct {
	char display[9];
	void (*up_program)(void);
	void (*down_program)(void);
	void (*enter_program)(void);
} menu_item_t;

static menu_item_t menu_item[MENU_PAGE_NUMBER];

void menu_init(void);
void menu_active(void);

#endif /* MENU_H_ */
