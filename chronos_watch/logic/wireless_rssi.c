/*
 * wireless_rssi.c
 *
 *  Created on: 2014骞�2鏈�鏃�
 *      Author: Code
 */


#include "project.h"
#include "wireless_rssi.h"

//driver
#include "display.h"
#include "radio.h"
#include "pmm.h"
#include "bmp_as.h"
#include "statedef.h"
#include "rf1a.h"

// *************************************************************************************************
// @fn          wireless_rssi_active
// @brief       main routine.
// @param       none
// @return      none
// *************************************************************************************************
void wireless_rssi_active(void)
{
	display_chars(0, 9, "MO@RP@@SN", SEG_ON);	// up: Single mode; down: Repeat mode; left: motion sensor mode
	LCDBBLKCTL |= LCDBLKMOD0; // start blink
    // ---------------------------------------------------------------------
    // Reset radio core
	// Set Vcore to 2 , which is needed by radio core all the time. See errata.
    SetVCore(2);
    radio_init();

    u8 current_mode = 0;	//mode selection. 0: off; 1: single; 2: repeat; 3: motion
    s8 set_enable = 0;			//mode. 1:enable. -1:disable. 0:none
	// loop stage
    u8 module_loop = 1;
    while(module_loop) {
        //Global process
        process_global_event();

    	//clear radio state
		if(radio_state == RADIO_TX_FINISHED) {
			radio_state = RADIO_IDLE;
			radio_setidle(0);
		}
		if(radio_state == RADIO_IDLE) {
	        // When idle go to LPM3
	   		_BIS_SR(LPM3_bits + GIE);
		}

		//process key event
        if(key_status == BUTTON_UP_IV) {
        	if(current_mode == 0) {
        		current_mode = 1;
        	}
        } else if(key_status == BUTTON_DOWN_IV) {
        	if(current_mode == 0) {
        		current_mode = 2;
        		set_enable = 1;
        		display_chars(4, 2, "RP", BLINK_ON);
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_ON);
        	} else {
        		set_enable = -1;
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_OFF);
        	}
        } else if(key_status == BUTTON_ENTER_IV) {
        	if(current_mode == 0) {
        		current_mode = 3;
        		set_enable = 1;
        		display_chars(7, 2, "MO", BLINK_ON);
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_ON);
        		write_lcd_mem(LCD_ICON_BEEPER2_MEM, LCD_ICON_BEEPER2_MASK, LCD_ICON_BEEPER2_MASK, SEG_ON);
        	} else {
        		set_enable = -1;
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_OFF);
        		write_lcd_mem(LCD_ICON_BEEPER2_MEM, LCD_ICON_BEEPER2_MASK, LCD_ICON_BEEPER2_MASK, SEG_OFF);
        	}
        } else if(key_status == BUTTON_ESC_IV) {
        	if(current_mode == 0) {
				//exit program
				current_program = clock_active;
				module_loop = 0;
        	} else {
        		set_enable = -1;
        		write_lcd_mem(LCD_ICON_BEEPER1_MEM, LCD_ICON_BEEPER1_MASK, LCD_ICON_BEEPER1_MASK, SEG_OFF);
        		write_lcd_mem(LCD_ICON_BEEPER2_MEM, LCD_ICON_BEEPER2_MASK, LCD_ICON_BEEPER2_MASK, SEG_OFF);
        	}
        }
        key_status = 0;

		//mode selection
    	if(current_mode == 1) {
			if(radio_state == RADIO_IDLE) {
				set_enable = -1;		//set mode
				//single shot
				rf1a_txbuffer[0] = 2;	//length
				rf1a_txbuffer[1] = RADIO_OPERATION_RSSI;	//package type
				rf1a_txbuffer[2] = 0;	//single ping
				radio_settx();
			}
			current_mode = 0;
    	} else if(current_mode == 2) {
    		if(set_enable == 1) {
    			set_enable = 0;
    			RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
    			rtc_user.flag.radio = 1;	//enable clock mode
                //repeat shot
    			rf1a_txbuffer[0] = 2;	//length
    			rf1a_txbuffer[1] = RADIO_OPERATION_RSSI;	//package type
               	rf1a_txbuffer[2] = 1;	//continuous ping
    		} else if(set_enable == 0 && sysflags.flag.clock_sec) {
    			sysflags.flag.clock_sec = 0;
    			if(radio_state == RADIO_IDLE) {
    				radio_settx();
    			}
    		} else if(set_enable == -1) {
    			rtc_user.flag.radio = 0;	//remove rtc usage
    			set_enable = 0;
    			current_mode = 0;
    			LCDBMEMCTL |= LCDCLRBM;		//clear blinking memory
    		}
    	} else if(current_mode == 3) {
    		if(set_enable == 1) {
    			set_enable = 0;
    			rf1a_txbuffer[0] = 2;	//length
    			rf1a_txbuffer[1] = RADIO_OPERATION_RSSI;	//package type
               	rf1a_txbuffer[2] = 2;	//motion detection ping

               	bmp_as_start(BMP_AS_MODE_ANYMOTION);
    			RTCCTL01 |= RTCRDYIE;	//enable 1hz interrupt
    			rtc_user.flag.radio = 1;	//enable clock mode
    		} else if(set_enable == 0 && sysflags.flag.clock_sec) {
    			sysflags.flag.clock_sec = 0;
    	        if(sysflags.flag.as_ready) {
    	        	sysflags.flag.as_ready = 0;
					if(radio_state == RADIO_IDLE) {
						radio_settx();
					}
    	        }
    		} else if(set_enable == -1) {
    			bmp_as_stop();
    			rtc_user.flag.radio = 0;	//remove rtc usage
    			set_enable = 0;
    			current_mode = 0;
    			LCDBMEMCTL |= LCDCLRBM;		//clear blinking memory
    		}
    	}

    }

    // Cleanup
    LCDBBLKCTL &= ~LCDBLKMOD0; // stop blink
	// Reset Vcore, which will also disable radio core. See errata.
    SetVCore(1);
}
