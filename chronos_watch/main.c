// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//******************************************************************************
//   eZ430-Chronos
//
//   Description: This is the standard software for the eZ430-Chronos
//
//   Version    1.7
//**********************************************************************************

// *************************************************************************************************
// Initialization and control of application.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"

// driver
#include "display.h"

// logic
#include "menu.h"
#include "global.h"

// *************************************************************************************************
// Global Variable section
volatile s_system_flags sysflags;
volatile s_system_status sysstats;
volatile s_rtc_user rtc_user;
volatile u8 radio_state = 1;	//radio state machine
u8 current_day = DEFAULT_DAY;
u16 key_status_temp;
u16 key_status;
u8 menu_page;
timer_preset_t current_timer;	//for timer
timer_preset_t stopwatch_timer;	//for stopwatch
u16 adc12_result;
u16 buzzer_cycle;	//timer cycle, used for buzzer set

// *************************************************************************************************
// SPI buffer Defines section
u8 spi_txbuffer[2];
u8 spi_rxbuffer[2];

u8 rf1a_txbuffer[64];
u8 rf1a_rxbuffer[64];

// *************************************************************************************************
// Extern section
void (*current_program)(void);

// *************************************************************************************************
// @fn          main
// @brief       Main routine
// @param       none
// @return      none
// *************************************************************************************************
int main(void)
{
    // Hold watchdog
    // Watchdog triggers after 250 ms when not cleared
    // WDTCTL = WDTPW + WDTCNCTL + WDTTMSEL + WDTIS__8192 + WDTSSEL__ACLK;
    WDTCTL = WDTPW + WDTHOLD;
    SFRIE1 |= WDTIE;

    // Init MCU
    mcu_init();

	//First module;
	menu_init();	//init menu system
	menu_page = MENU_PAGE_NUMBER;	//reset to clock page
	current_program = MENU_DEFAULT_ENTRY;

    // Main control loop: wait in low power mode until some event needs to be processed
    while (1)
    {
    	clear_display();
    	(*current_program)();
    }
}

