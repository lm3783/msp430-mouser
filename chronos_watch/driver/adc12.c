// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// ADC12 functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include <project.h>

// driver
#include "adc12.h"

// *************************************************************************************************
// @fn          adc12_single_conversion
// @brief       Init ADC12.      Do single measurement of battery voltage. Turn off ADC12.
// @param       none
// @return      none
// *************************************************************************************************
void adc12_single_conversion(void)
{
    // Initialize the shared reference module
    REFCTL0 |= REFMSTR + REFVSEL_1 + REFON + REFTCOFF;   // Enable internal reference (1.5V or 2.5V)

    // Initialize ADC12_A
    ADC12CTL0 = ADC12SHT0_10 + ADC12ON;          // Set sample time
    ADC12CTL1 = ADC12SHP;		    // Enable sample timer
    // Convert external battery voltage (ADC12INCH_11=AVCC-AVSS/2)
    ADC12MCTL0 = ADC12SREF_1 + ADC12INCH_11; // ADC input channel
    ADC12IE = 0x001;                    // ADC_IFG upon conv result-ADCMEMO
    // Start ADC12
    ADC12CTL0 |= (ADC12ENC | ADC12SC);	// Sampling and conversion start
}

// *************************************************************************************************
// @fn          ADC12ISR
// @brief       Store ADC12 conversion result. Set flag to indicate data ready.
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector=ADC12_VECTOR
__interrupt void ADC12ISR(void)
{
    if(ADC12IV == 6) {
        // Vector  6:  ADC12IFG0
		adc12_result = ADC12MEM0; // Move results, IFG is cleared
		sysflags.flag.adc12_data_ready = 1;

		// Shut down ADC12
		ADC12CTL0 &= ~(ADC12ENC | ADC12SC | ADC12SHT0_10);
		ADC12CTL0 &= ~ADC12ON;

		// Shut down reference voltage
		REFCTL0 &= ~(REFMSTR + REFVSEL_1 + REFON + REFTCOFF);

		ADC12IE = 0;

		_BIC_SR_IRQ(LPM3_bits);   // Exit active CPU
    }
}

