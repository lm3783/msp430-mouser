/*
 * rtc.c
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

// system
#include "project.h"
#include "rtcisr.h"

// *************************************************************************************************
// @fn          RTC_A_ISR
// @brief       IRQ handler for RTC.
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = RTC_A_VECTOR
__interrupt void RTC_A_ISR(void)
{
    u16 iv = RTCIV;
    if(iv == RTCIV_RTCRDYIFG) {
    	sysflags.flag.clock_sec = 1;
    	if(sysstats.flag.backlight_enabled) {	//backlight function
    		if(backlight_on_seconds < BACKLIGHT_TIME_ON) {
    			backlight_on_seconds++;
    		} else {
    			P2OUT &= ~BUTTON_BACKLIGHT_PIN;
    			P2DIR &= ~BUTTON_BACKLIGHT_PIN;
    			backlight_on_seconds = 0;
    			rtc_user.flag.backlight = 0;		//remove rtc usage
    		}
    	}
    	if(sysstats.flag.buzzer_enabled) {		//buzzer function
    		buzzer_cycle = 0;	//clear counter
    		// Allow buzzer PWM output on P2.7
    		P2SEL |= BIT7;
    		// Activate Timer periodic interrupts
    		TA1CCTL2 |= CCIE;	//enable interrupt
    		// Reset TA1R, set up mode, TA1 runs from 32768Hz ACLK
    		TA1CTL |= TASSEL__ACLK | MC__UP | TACLR;
    	}
    	if(sysstats.flag.timer_enabled) {	//timer function
    		current_timer.second--;
    		if(current_timer.second == 0xFF) {
    			current_timer.second = 59;
    			current_timer.minute--;
    			if(current_timer.minute == 0xFF) {
    				current_timer.minute = 59;
    				current_timer.hour--;
    				if(current_timer.hour == 0xFF) {
    					current_timer.hour = 0;		//clear counter
    					current_timer.minute = 0;
    					current_timer.second = 0;
    					sysstats.flag.timer_enabled = 0;	//stop timer
    					rtc_user.flag.timer = 0;		//disable interrupt
    					sysflags.flag.timer_alarm = 1;	//generate alarm
    				}
    			}
    		}
    	}
    	if(!(rtc_user.all_flags)) {
    		RTCCTL01 &= ~RTCRDYIE;      // disable rtc second interrupt
    	}
    } else if(iv == RTCIV_RTCTEVIFG) {
    	// This interrupt could be per minute or per day.
    	sysflags.flag.clock_min = 1;
    	if(current_day != RTCDAY) {
    		sysflags.flag.clock_day = 1;
    	}
    } else if(iv == RTCIV_RTCAIFG) {
    	//alarm interrupt
    	sysflags.flag.clock_alarm = 1;
    }
    _BIC_SR_IRQ(LPM3_bits);
}
