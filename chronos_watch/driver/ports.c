// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Button entry functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"

// driver
#include "ports.h"
#include "display.h"

// *************************************************************************************************
// Prototypes section

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section


// *************************************************************************************************
// @fn          PORT2_ISR
// @brief       Interrupt service routine for
//                                      - buttons
//                                      - acceleration sensor INT1
//                                      - pressure sensor EOC
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector=PORT2_VECTOR
__interrupt void PORT2_ISR(void)
{
	key_status = P2IV;
	switch(key_status) {
	case P2IV_P2IFG3:
		//backlight
		sysstats.flag.backlight_enabled = 1;	//set flag
		RTCCTL01 |= RTCRDYIE;	//enable RTC interrupt
		rtc_user.flag.backlight = 1;		//add usage
		P2OUT |= BUTTON_BACKLIGHT_PIN;	//enable backlight
		P2DIR |= BUTTON_BACKLIGHT_PIN;
		return;
	case P2IV_P2IFG5:
		// Get data from Acceleration sensor
		sysflags.flag.as_ready = 1;
		break;
	case P2IV_P2IFG6:
		// Get data from Pressure sensor
		sysflags.flag.ps_ready = 1;
		break;
	default:
#ifndef DEBUG_MODE
		// Debounce delay
		// Disable PORT2 IRQ
		BUTTONS_IE &= ~ALL_BUTTONS;
		// Debounce delay 1
		// Watchdog triggers after 250 ms when not cleared
		WDTCTL = WDTPW + WDTCNTCL + WDTTMSEL + WDTIS__8192 + WDTSSEL__ACLK;
#endif
		if(sysstats.flag.buzzer_enabled) {
			//Disable alarm check;
			sysstats.flag.buzzer_enabled = 0;
			//remove RTC usage
			rtc_user.flag.buzzer = 0;
			key_status = 0;		//do not output key status
			return;
		}
	}
	_BIC_SR_IRQ(LPM3_bits);
}

// *************************************************************************************************
// @fn          WDT_ISR
// @brief       Interrupt service routine for
//                                      - buttons delay
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector=WDT_VECTOR
__interrupt void WDT_ISR(void)
{
	WDTCTL = WDTPW + WDTHOLD;       //Hold Watchdog
	u8 btn = BUTTONS_IN;
	if((btn & BUTTON_STAR_PIN) && (btn & BUTTON_UP_PIN)) {
		//press ESC and UP to reset
		WDTCTL = 0;
	}
	// Clear all flags
	BUTTONS_IFG &= ~ALL_BUTTONS;
	BUTTONS_IE |= ALL_BUTTONS;      //Enable PORT2 IRQ
	BUTTONS_IFG |= (btn & ALL_BUTTONS);		//Repeat
}
