/*
 * ta.c
 *
 *  Created on: 2014年8月10日
 *      Author: Code
 */

// system
#include "project.h"
#include "ta.h"

// *************************************************************************************************
// @fn          Timer1_A1_Delay
// @brief       Wait for some microseconds
// @param       ticks (1 tick = 1/32768 sec)
// @return      none
// *************************************************************************************************
void Timer1_A1_Delay(u16 ticks)
{
    // ---------------------------------------------------------------------
    // Configure Timer1 for use by the clock and delay functions
    // Set interrupt frequency to 1Hz
    TA1CCR0 = 32768 - 1;

    TA1CCR1 = ticks;	//set dest
    TA1CCTL1 |= CCIE;	//enable interrupt

    TA1CTL |= TASSEL__ACLK | MC__UP | TACLR;	//start timer

    // loop stage
    u8 module_loop = 1;
    while(module_loop) {
        // When idle go to LPM3
        _BIS_SR(LPM3_bits + GIE);

        if(sysflags.flag.ta_timeup) {
        	sysflags.flag.ta_timeup = 0;
        	module_loop = 0;
        }
    }
}

// *************************************************************************************************
// @fn          TIMER0_A1_ISR
// @brief       IRQ handler for TIMER0_A1 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_5_ISR(void)
{
	u16 timeriv = TA0IV;
	if(sysstats.flag.stopwatch_enabled && timeriv == 0x0E) {
		stopwatch_timer.second++;
		if(stopwatch_timer.second == 60) {
			stopwatch_timer.second = 0;
			stopwatch_timer.minute++;
			if(stopwatch_timer.minute == 60) {
				stopwatch_timer.minute = 0;
				stopwatch_timer.hour++;
				if(stopwatch_timer.hour == 24) {
					//stop stopwatch
					sysstats.flag.stopwatch_enabled = 0;
					TA0CTL &= ~MC__UP;
				}
			}
		}
	}
	sysflags.flag.clock_sec = 1;
	_BIC_SR_IRQ(LPM3_bits);
}

// *************************************************************************************************
// @fn          TIMER1_A1_ISR
// @brief       IRQ handler for TIMER1_A1 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER1_A1_VECTOR
__interrupt void TIMER1_A1_3_ISR(void)
{
	u16 timeriv = TA1IV;
	if(timeriv == 0x02) {
		sysflags.flag.ta_timeup = 1;
	    //stop ta1 timer
	    TA1CTL &= ~MC__UP;
	    TA1CCTL1 &= ~CCIE;	//disable interrupt
		_BIC_SR_IRQ(LPM3_bits);
	} else if(timeriv == 0x04) {
		//buzzer timeout
		buzzer_cycle++;
		if(buzzer_cycle >= BUZZER_ON_CYCLES) {
		    //stop ta1 timer
		    TA1CTL &= ~MC__UP;
		    TA1CCTL2 &= ~CCIE;	//disable interrupt

		    // Reset and disable buzzer PWM output
	        P2OUT &= ~BIT7;
	        P2SEL &= ~BIT7;
		}
	}
}

// *************************************************************************************************
// @fn          TIMER1_A0_ISR
// @brief       IRQ handler for TIMER1_A0 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
}
