// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************

// *************************************************************************************************
// Radio state machine.
// *************************************************************************************************

// *************************************************************************************************
// Include section
#include "project.h"
#include "radio.h"
#include "statedef.h"
#include "rf1a.h"

// *************************************************************************************************
// Define section
// The original rf1a driver is used, since DMA is useless on radio core. See TI forum

u8 radio_setidle(u8 mode)
{
	u8 ret;
	ret = Strobe(RF_SPWD);
	if(mode) {
		Strobe(RF_SFTX);
		Strobe(RF_SFRX);
	}
	if(mode == 2) {
		WriteSingleReg(WORCTRL,0xFB);
		RF1AIE &= ~(BITE | BIT1);
	}
	radio_state = RADIO_IDLE;
	return ret;
}

u8 radio_settx(void)
{
	radio_state = RADIO_TX_TRANSMITTING;
	u8 ret;
	WriteBurstReg(TXFIFO, rf1a_txbuffer, rf1a_txbuffer[0] + 1);
	RF1AIFG &= ~BIT9;
	RF1AIE |= BIT9;		//enable RFIFG9
	ret = Strobe(RF_STX);
	return ret;
}

u8 radio_setrx(void)
{
	u8 ret;
	radio_state = RADIO_RX_RECEIVING;
	RF1AIFG &= ~BIT9;
	RF1AIE |= BIT9;		//enable RFIFG9
	ret = Strobe(RF_SRX);
	return ret;
}

u8 radio_getrx(void)
{
	u8 ret;
	u8 length = ReadSingleReg(RXBYTES);
	if(length) {
		ReadBurstReg(RXFIFO, rf1a_rxbuffer, length);
	} else {
		return 0;
	}
	length = rf1a_rxbuffer[0];
	ret = rf1a_rxbuffer[length + 2];
	if(ret & BIT7) {
		return 1;
	} else {
		return 0;
	}
}

u8 radio_setrxwor(u8 init)
{
	u8 ret;
	if(init) {
	  WriteSingleReg(WOREVT1, 0x87);
	  WriteSingleReg(WOREVT0, 0x6B);
	  /* WORCTRL.EVENT1 = 7 => T_EVENT1 = ~1.5 msec */
	  WriteSingleReg(MCSM2, 0x00);
	  /* WORCTRL.WOR_RES = 0 (EVENT0 = 34667, RX_TIME = 6)
		 =>  RX timeout  = 34667 * 0.0563 = ~1.96 msec */
	  WriteSingleReg(WORCTRL, (7 << 4) | 0);
	}
	RF1AIE |= BITE;		//enable RFIFG14 (wake up signal)
	radio_state = RADIO_IDLE;	//wor is also set to idle
	ret = Strobe(RF_SWOR);
	return ret;
}

void radio_init(void)
{
	// reset radio core
	ResetRadioCore();
	// set radio param
	WriteRfSettings();
	// enable irq
	RF1AIES |= BIT9;		//high-to-low transition
	RF1AIFCTL1 |= RFERRIE;		// enable interrupt to controll DMA
	//init radio state
	radio_state = RADIO_IDLE;
}

// *************************************************************************************************
// @fn          CC1101_ISR
// @brief       IRQ handler for CC1101 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = CC1101_VECTOR
__interrupt void CC1101_ISR(void)
{
	u16 rfiv = RF1AIFERRV;
	if(rfiv == RF1AIFERRV_LVERR) {
		RF1AIFERR = 0;		//clear error state
	}
	rfiv = RF1AIV;	//clear IV and IFG
	if(rfiv == RF1AIV_RFIFG9) {
		if(radio_state == RADIO_TX_TRANSMITTING) {
			radio_state = RADIO_TX_FINISHED;
		} else if(radio_state == RADIO_RX_RECEIVING) {
			radio_state = RADIO_RX_FINISHED;
		}
		RF1AIE &= ~(BIT9);		//disable RFIFG9
		_BIC_SR_IRQ(LPM3_bits);		//exit LPM
	} else if(rfiv == RF1AIV_RFIFG14) {
		RF1AIE &= ~(BITE);
		RF1AIE |= BIT1;		//enable LNA_PD signal
		radio_state = RADIO_RX_WOR;
		_BIC_SR_IRQ(LPM3_bits);		//exit LPM
	} else if(rfiv == RF1AIV_RFIFG1) {
		RF1AIE &= ~(BIT1);
		radio_state = RADIO_RX_WOR_TIMEOUT;
		_BIC_SR_IRQ(LPM3_bits);		//exit LPM
	}
}
