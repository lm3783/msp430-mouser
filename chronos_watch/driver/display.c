// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Display functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include <project.h>
#include <string.h>

// driver
#include "display.h"

// logic

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section
// Table with memory bit assignment for digits "0" to "9" and characters "A" to "Z"
const u8 lcd_font[] = {
    SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F,         // Displays "0"
    SEG_B + SEG_C,                                         // Displays "1"
    SEG_A + SEG_B + SEG_D + SEG_E + SEG_G,                 // Displays "2"
    SEG_A + SEG_B + SEG_C + SEG_D + SEG_G,                 // Displays "3"
    SEG_B + SEG_C + SEG_F + SEG_G,                         // Displays "4"
    SEG_A + SEG_C + SEG_D + SEG_F + SEG_G,                 // Displays "5"
    SEG_A + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G,         // Displays "6"
    SEG_A + SEG_B + SEG_C,                                 // Displays "7"
    SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G, // Displays "8"
    SEG_A + SEG_B + SEG_C + SEG_D + SEG_F + SEG_G,         // Displays "9"
    0,                                                     // Displays " "
    0,                                                     // Displays " "
    0,                                                     // Displays " "
    SEG_G,                                                 // Displays "-" use "="
    0,                                                     // Displays " "
    0,                                                     // Displays " "
    0,                                                     // Displays " " use "@"
    SEG_A + SEG_B + SEG_C + SEG_E + SEG_F + SEG_G,         // Displays "A"
    SEG_C + SEG_D + SEG_E + SEG_F + SEG_G,                 // Displays "b"
    SEG_A + SEG_D + SEG_E + SEG_F,                         // Displays "C"
    SEG_B + SEG_C + SEG_D + SEG_E + SEG_G,                 // Displays "d"
    SEG_A + +SEG_D + SEG_E + SEG_F + SEG_G,                // Displays "E"
    SEG_A + SEG_E + SEG_F + SEG_G,                         // Displays "F"
    SEG_A + SEG_C + SEG_D + SEG_E + SEG_F,                 // Displays "G"
    SEG_B + SEG_C + SEG_E + SEG_F + SEG_G,                 // Displays "H"
    SEG_E + SEG_F,                                         // Displays "I"
    SEG_B + SEG_C + SEG_D + SEG_E,                         // Displays "J"
    SEG_D + SEG_E + SEG_F + SEG_G,                         // Displays "k"
    SEG_D + SEG_E + SEG_F,                                 // Displays "L"
    SEG_A + SEG_C + SEG_E + SEG_G,                         // Displays "M"
    SEG_C + SEG_E + SEG_G,                                 // Displays "n"
    SEG_C + SEG_D + SEG_E + SEG_G,                         // Displays "o"
    SEG_A + SEG_B + SEG_E + SEG_F + SEG_G,                 // Displays "P"
    SEG_A + SEG_B + SEG_C + SEG_F + SEG_G,                 // Displays "q"
    SEG_E + SEG_G,                                         // Displays "r"
    SEG_A + SEG_C + SEG_D + SEG_F,                         // Displays "S"
    SEG_A + SEG_E + SEG_F,                                 // Displays "T"
    SEG_C + SEG_D + SEG_E,                                 // Displays "u"
    SEG_B + SEG_C + SEG_D + SEG_E + SEG_F,                 // Displays "V"
    SEG_B + SEG_D + SEG_F + SEG_G,                         // Displays "W"
    SEG_C + SEG_F + SEG_G,                                 // Displays "X"
    SEG_B + SEG_C + SEG_D + SEG_F + SEG_G,                 // Displays "Y"
    SEG_A + SEG_B + SEG_D + SEG_E,                         // Displays "Z"
};

// the array is from right to left, top to bottom.
const u8 *segments_lcdmem[] = {
    LCD_SEG_L1_0_MEM,
    LCD_SEG_L1_1_MEM,
    LCD_SEG_L1_2_MEM,
    LCD_SEG_L1_3_MEM,
    LCD_SEG_L2_0_MEM,
    LCD_SEG_L2_1_MEM,
    LCD_SEG_L2_2_MEM,
    LCD_SEG_L2_3_MEM,
    LCD_SEG_L2_4_MEM,
    LCD_SEG_L2_5_MEM,
};

const u8 segments_bitmask[] = {
    LCD_SEG_L1_0_MASK,
    LCD_SEG_L1_1_MASK,
    LCD_SEG_L1_2_MASK,
    LCD_SEG_L1_3_MASK,
    LCD_SEG_L2_0_MASK,
    LCD_SEG_L2_1_MASK,
    LCD_SEG_L2_2_MASK,
    LCD_SEG_L2_3_MASK,
    LCD_SEG_L2_4_MASK,
    LCD_SEG_L2_5_MASK,
};

const u8 int_to_array_conversion_table[][2] = {
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
    "13", "14", "15",
    "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
    "29", "30", "31",
    "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44",
    "45", "46", "47",
    "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
    "61", "62", "63",
    "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76",
    "77", "78", "79",
    "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92",
    "93", "94", "95",
    "96", "97", "98", "99",
};

// Global return string for int_to_array function

// *************************************************************************************************
// @fn          clear_display
// @brief       Erase LINE1 and LINE2 segments. Keep icons.
// @param       none
// @return      none
// *************************************************************************************************
void clear_display(void)
{
	LCDBMEMCTL |= LCDCLRBM;
	display_chars(0, 10, "", SEG_OFF);
	write_lcd_mem(LCD_SEG_L1_COL_MEM, LCD_SEG_L1_COL_MASK | LCD_SEG_L1_DP1_MASK | LCD_SEG_L2_COL1_MASK, LCD_SEG_L1_COL_MASK | LCD_SEG_L1_DP1_MASK | LCD_SEG_L2_COL1_MASK, SEG_OFF);  // :
	write_lcd_mem(LCD_SEG_L2_COL0_MEM, LCD_SEG_L2_COL0_MASK | LCD_SEG_L1_DP0_MASK | LCD_UNIT_L1_DEGREE_MASK, LCD_SEG_L2_COL0_MASK | LCD_SEG_L1_DP0_MASK | LCD_UNIT_L1_DEGREE_MASK, SEG_OFF);  // :
	write_lcd_mem(LCD_SEG_L2_DP_MEM, LCD_SEG_L2_DP_MASK, LCD_SEG_L2_DP_MASK, SEG_OFF);  // :
}

// *************************************************************************************************
// @fn          write_segment
// @brief       Write to one or multiple LCD segments
// @param       lcdmem          Pointer to LCD byte memory
//                              bits            Segments to address
//                              bitmask         Bitmask for particular display item
//                              mode            On, off or blink segments
// @return
// *************************************************************************************************
void write_lcd_mem(u8 * lcdmem, u8 bits, u8 bitmask, u8 state)
{
	if(state & SEG_OFF) {
		// Clear segments before writing
		*lcdmem = (u8) (*lcdmem & ~bitmask);
	}
    if (state & SEG_ON) {
        // Set visible segments
        *lcdmem = (u8) (*lcdmem | bits);
    }
    if(state & BLINK_OFF) {
    	*(lcdmem + LCD_MEM_BLINK_OFFSET) = (u8) (*(lcdmem + LCD_MEM_BLINK_OFFSET) & ~bitmask);
    }
    if (state & BLINK_ON) {
        // Set visible / blink segments
        *(lcdmem + LCD_MEM_BLINK_OFFSET) = (u8) (*(lcdmem + LCD_MEM_BLINK_OFFSET) | bits);
    }
}

// *************************************************************************************************
// @fn          display_char
// @brief       Write to 7-segment characters.
// @param       u8 segment              A valid LCD segment
//                              u8 chr                  Character to display
//                              u8 mode         SEG_ON, SEG_OFF, SEG_BLINK
// @return      none
// *************************************************************************************************
void display_char(u8 segment, u8 chr, u8 mode)
{
    u8 *lcdmem;                 // Pointer to LCD memory
    u8 bitmask;                 // Bitmask for character
    u8 bits, bits1;             // Bits to write

	// Get LCD memory address for segment from table
	lcdmem = (u8 *) segments_lcdmem[segment];
	// Get bitmask for character from table
	bitmask = segments_bitmask[segment];
	// Get bits from font set
	if ((chr >= 0x30) && (chr <= 0x5A))
	{
		// Use font set
		bits = lcd_font[chr - 0x30];
	}
	else
	{
		// Other characters map to ' ' (blank)
		bits = 0;
	}

	// When addressing LINE2 7-segment characters need to swap high- and low-nibble,
	// because LCD COM/SEG assignment is mirrored against LINE1
    if (segment >= LCD_L2)
	{
		bits1 = ((bits << 4) & 0xF0) | ((bits >> 4) & 0x0F);
		bits = bits1;

		// When addressing LCD_SEG_L2_5, need to convert ASCII '1' and 'L' to 1 bit,
		// because LCD COM/SEG assignment is special for this incomplete character
		if (segment == 9) {
			if(chr == '1') {
				bits = BIT7;
			} else {
				bits = 0;
			}
		} else if(segment > 9) {
	    	return;		//overflow
	    }
	}

	// Physically write to LCD memory
	write_lcd_mem(lcdmem, bits, bitmask, mode );
}


// *************************************************************************************************
// @fn          display_chars
// @brief       Write to consecutive 7-segment characters.
// @param       u8 start     LCD start segment (right most)
//              u8 length    number of characters
//                              u8 * str                Pointer to a string
//                              u8 mode             SEG_ON, SEG_OFF, SEG_BLINK
// @return      none
// *************************************************************************************************
void display_chars(u8 start, u8 length, char * str, u8 mode)
{
    u8 i;
    if(mode == SEG_OFF) {
    	for(i = start + length - 1; i >= start ; i--) {
    		write_lcd_mem((u8 *)segments_lcdmem[i], segments_bitmask[i], segments_bitmask[i], SEG_OFF);
    	}
    } else {
    	start -= 1;
    	str += length;
    	// Write to consecutive digits
    	for (i = length; i ; i--)
    	{
    		// Use single character routine to write display memory
    		display_char(start + i, *(str - i), mode);
    	}
    }
}

// *************************************************************************************************
// @fn          display_alarm_value
// @brief       Generic decimal display routine. Used exclusively by set_value function.
// @param       u8 start     LCD start segment (right most)
//                              u8 value                value to be displayed (<100)
//                              u8 mode             SEG_ON, SEG_OFF, SEG_BLINK
//                              u8 blanks           blank mode. 0: no blank, 1 blank
// @return      none
// *************************************************************************************************
void display_alarm_value(u8 start, u8 value, u8 mode, u8 blanks)
{
	if(value & BIT7) {
		value &= ~BIT7;
		display_value(start, value, mode, blanks);
	} else {
		display_chars(start, 2, "==", mode);
	}
}

// *************************************************************************************************
// @fn          display_value
// @brief       Generic decimal display routine. Used exclusively by set_value function.
// @param       u8 start     LCD start segment (right most)
//                              u8 value                value to be displayed (<100)
//                              u8 mode             SEG_ON, SEG_OFF, SEG_BLINK
//                              u8 blanks           blank mode. 1:show '0' placeholder
// @return      none
// *************************************************************************************************
void display_value(u8 start, u8 value, u8 mode, u8 blanks)
{
	if(!blanks && value < 10) {
    	display_char(start, value + 0x30, mode);
    	start += 1;
   		write_lcd_mem((u8 *)segments_lcdmem[start], segments_bitmask[start], segments_bitmask[start], SEG_OFF);
	} else {
    	memcpy(int_to_array_str, int_to_array_conversion_table[value], 2);
    	display_chars(start, 2, int_to_array_str, mode);
	}
}

// *************************************************************************************************
// @fn          display_values
// @brief       Generic decimal display routine. Used exclusively by set_value function.
// @param       u8 start     LCD start segment (right most)
//              u8 length    number of characters
//                              s32 value                value to be displayed
//                              u8 mode             SEG_ON, SEG_OFF, SEG_BLINK
// int_to_array result string
// @return      none
// *************************************************************************************************
void display_values(u8 start, s32 value, u8 mode)
{
    u8 minus = 0;
    u8 i = 0;
    u8 j = 0;
    u8 k = 0;
    if(value < 0) {
        minus = 1;
        value = 0 - value;
    }

    while(value >= 10000) {
        value -= 10000;
        i++;
    }
    if(i) {
    	memcpy(int_to_array_str, int_to_array_conversion_table[i], 2);
    	k += 2;
    }

    j = 0;
    while(value >= 100) {
        value -= 100;
        j++;
    }
    if(i || j) {
    	memcpy(int_to_array_str + k, int_to_array_conversion_table[j], 2);
    	k += 2;
    }

    memcpy(int_to_array_str + k, int_to_array_conversion_table[value], 2);

   //display
    display_chars(start, k + 2, int_to_array_str, mode);
    //sign
    if(minus) {
        if(start < LCD_L2) {
            display_char(3, '=', mode);
        } else {
        	write_lcd_mem((u8 *)segments_lcdmem[9], segments_bitmask[9], segments_bitmask[9], SEG_OFF);
            display_char(8, '=', mode);
        }
    }
}

// *************************************************************************************************
// @fn          change_value
// @brief       Display a value and let user change it.
// @param       u8 position     LCD start segment (right most)
//                              u8 current          default value
//                              u8 min              min value
//                              u8 max              max value
//								u8 special			special mode(for alarm setting)
// Key function: up: increase value
//				down: decrease value
//				enter:	enter value
//				esc: quit
// @return      u8 final value. value = 0xFF means quit
// *************************************************************************************************
u8 change_value(u8 position, u8 current, u8 min, u8 max, u8 special)
{
	// Clear blinking memory
	LCDBMEMCTL |= LCDCLRBM;
	u8 loop = 1;
	if(special) {		//deal with alarm value
		current &= ~BIT7;
	}
	while(loop) {
		display_value(position, current, SEG_OFF | SEG_ON | BLINK_OFF | BLINK_ON, 1);
		// When idle go to LPM3
		_BIS_SR(LPM3_bits + GIE);

		if(key_status == BUTTON_UP_IV) {
			if(current == max) {
				current = min;
			} else {
				current++;
			}
		} else if(key_status == BUTTON_DOWN_IV) {
			if(current == min) {
				current = max;
			} else {
				current--;
			}
		} else if(key_status == BUTTON_ENTER_IV) {
			loop = 0;
		} else if(key_status == BUTTON_ESC_IV) {
			loop = 0;
			current = 0xff;
			special = 0;
		}
		key_status = 0;
	}
	if(special) {
		current |= BIT7;
	}
	return current;
}
