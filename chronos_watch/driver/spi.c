// *************************************************************************************************
//
//	Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/ 
//	 
//	 
//	  Redistribution and use in source and binary forms, with or without 
//	  modification, are permitted provided that the following conditions 
//	  are met:
//	
//	    Redistributions of source code must retain the above copyright 
//	    notice, this list of conditions and the following disclaimer.
//	 
//	    Redistributions in binary form must reproduce the above copyright
//	    notice, this list of conditions and the following disclaimer in the 
//	    documentation and/or other materials provided with the   
//	    distribution.
//	 
//	    Neither the name of Texas Instruments Incorporated nor the names of
//	    its contributors may be used to endorse or promote products derived
//	    from this software without specific prior written permission.
//	
//	  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//	  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//	  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//	  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//	  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//	  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//	  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//	  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//	  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//	  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//	  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// acceleration sensor driver functions
// *************************************************************************************************


// *************************************************************************************************
// Include section

// system
#include "project.h"
#include "spi.h"

// driver
#include "ta.h"
// *************************************************************************************************
// Prototypes section

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section

// Global flag for proper acceleration sensor operation

// *************************************************************************************************
// Extern section


// *************************************************************************************************
// @fn          as_start
// @brief       Power-up and initialize acceleration sensor
// @param       none
// @return      none
// *************************************************************************************************
void as_start(void)
{
    // Initialize interrupt pin for data read out from acceleration sensor
    AS_INT_IES &= ~AS_INT_PIN;                   // Interrupt on rising edge

    // Enable interrupt
    AS_INT_DIR &= ~AS_INT_PIN;                   // Switch INT pin to input
    AS_SPI_DIR &= ~AS_SDI_PIN;                   // Switch SDI pin to input
    AS_SPI_REN |= AS_SDI_PIN;                    // Pulldown on SDI pin
    AS_SPI_SEL |= AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN; // Port pins to SDO, SDI and SCK function
    AS_CSN_OUT |= AS_CSN_PIN;                    // Deselect acceleration sensor
    AS_PWR_OUT |= AS_PWR_PIN;                    // Power on active high

    // Delay of >5ms required between switching on power and configuring sensor
    Timer1_A1_Delay(164);

	//Init DMA engine
	//Channel 0 is used for Tx
	//Channel 1 is used for Rx
    //Channel 2 is used for RF1A module(software controlled)
	DMACTL0 = DMA0TSEL_17 | DMA1TSEL_16 | DMA2TSEL_0;

	//Tx: single transfer, source increase, byte mode
	DMA0CTL = DMASRCINCR_3 | DMADSTBYTE | DMASRCBYTE | DMALEVEL;
	//source address
	DMA0SAL = (unsigned int) spi_txbuffer;                    // Source block address
	DMA0DAL = (unsigned int) &UCA0TXBUF;                   // Destination single address
	DMA0SZ = DMA_SPI_DATA_LENGTH;

	//Rx: repeat single transfer, destination increase, byte mode, interrupt enable
	DMA1CTL = DMADSTINCR_3 | DMADSTBYTE | DMASRCBYTE | DMALEVEL | DMAIE;
	//dest address
	DMA1SAL = (unsigned int) &UCA0RXBUF;                     // Source block address
	DMA1DAL = (unsigned int) spi_rxbuffer;                      // Destination single address
	DMA1SZ = DMA_SPI_DATA_LENGTH;

}

// *************************************************************************************************
// @fn          as_stop
// @brief       Power down acceleration sensor
// @param       none
// @return      none
// *************************************************************************************************
void as_stop(void)
{

    // Power-down sensor
    AS_PWR_OUT &= ~AS_PWR_PIN;                   // Power off
    AS_INT_OUT &= ~AS_INT_PIN;                   // Pin to low to avoid floating pins
    AS_SPI_OUT &= ~(AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN); // Pins to low to avoid floating pins
    AS_SPI_SEL &= ~(AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN); // Port pins to I/O function
    AS_CSN_OUT &= ~AS_CSN_PIN;                   // Pin to low to avoid floating pins
    AS_INT_DIR |= AS_INT_PIN;                    // Pin to output to avoid floating pins
    AS_SPI_DIR |= AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN; // Pins to output to avoid floating pins
    AS_CSN_DIR |= AS_CSN_PIN;                    // Pin to output to avoid floating pins
}

// *************************************************************************************************
// @fn          as_dma_access
// @brief               Communicate with SPI bus using DMA engine
// @param       u8* txAddress                     pointer to the tx array
//              u8* rxAddress                     pointer to the rx array
//				u8 mode							  transfer mode
// @return      u8 0 or bResult         Register content.
// *************************************************************************************************
u8 as_dma_access(u8 address, u8 value, u8 mode)
{
	u8 result;

	if(mode == DMA_SPI_WRITE) {
		address &= ~BIT7;
	} else {
		address |= BIT7;
		value = 0;
	}
	spi_txbuffer[0] = address;
	spi_txbuffer[1] = value;

	//init transfer
    AS_SPI_REN &= ~AS_SDI_PIN;                   // Pulldown on SDI pin not required
    AS_CSN_OUT &= ~AS_CSN_PIN;                   // Select acceleration sensor

    DMA0CTL |= DMAEN;							// start transfer
    DMA1CTL |= DMAEN;

    // When idle go to LPM3
    _BIS_SR(LPM3_bits + GIE);

    //cleanup
    AS_CSN_OUT |= AS_CSN_PIN;                    // Deselect acceleration sensor
    AS_SPI_REN |= AS_SDI_PIN;                    // Pulldown on SDI pin required again

    result = spi_rxbuffer[1];
    return result;
}

// *************************************************************************************************
// @fn          DMA_ISR
// @brief       IRQ handler for DMA IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = DMA_VECTOR
__interrupt void DMA_ISR(void)
{
	u16 dmaiv = DMAIV;	//clear IV and IFG
	if(dmaiv == 0x04) {		//DMA1, which means SPI finished
		_BIC_SR_IRQ(LPM3_bits);
	}

}
