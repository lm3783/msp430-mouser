// *************************************************************************************************
//
//	Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/ 
//	 
//	 
//	  Redistribution and use in source and binary forms, with or without 
//	  modification, are permitted provided that the following conditions 
//	  are met:
//	
//	    Redistributions of source code must retain the above copyright 
//	    notice, this list of conditions and the following disclaimer.
//	 
//	    Redistributions in binary form must reproduce the above copyright
//	    notice, this list of conditions and the following disclaimer in the 
//	    documentation and/or other materials provided with the   
//	    distribution.
//	 
//	    Neither the name of Texas Instruments Incorporated nor the names of
//	    its contributors may be used to endorse or promote products derived
//	    from this software without specific prior written permission.
//	
//	  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//	  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//	  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//	  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//	  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//	  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//	  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//	  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//	  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//	  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//	  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Bosch BMA250 acceleration sensor driver functions
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"

// logic

// driver
#include "bmp_as.h"
#include "spi.h"
#include "bma250.h"

// *************************************************************************************************
// @fn          bmp_as_start
// @brief       Power-up and initialize acceleration sensor
// @param       mode	sensor mode.
// @return      none
// *************************************************************************************************
void bmp_as_start(u8 mode)
{
    // Configure interface pins
    as_start();
	
	// Configure sensor and start to sample data
	// write sensor configuration
	// Set measurement range
	as_dma_access(BMA250_G_RANGE, BMA250_G_RANGE_2G, DMA_SPI_WRITE);
	// Set filter bandwidth
	as_dma_access(BMA250_BANDWIDTH, BMA250_BANDWIDTH_62HZ, DMA_SPI_WRITE);
	// Set power mode
	as_dma_access(BMA250_POWERMODE, BMA250_POWERMODE_10MS, DMA_SPI_WRITE);

	// configure sensor interrupt

	if(mode == BMP_AS_MODE_NORMAL) {
		// enable new data interrupt
		as_dma_access(BMA250_ISR2, BMA250_DATA_EN, DMA_SPI_WRITE);
		// map new data interrupt to INT1 pin
		as_dma_access(BMA250_IMR2, BMA250_DATA_INT1, DMA_SPI_WRITE);
	} else if(mode == BMP_AS_MODE_ANYMOTION) {
		// enable slope detection interrupt
		as_dma_access(BMA250_ISR1, BMA250_SLOPE_EN_X | BMA250_SLOPE_EN_Y | BMA250_SLOPE_EN_Z, DMA_SPI_WRITE);
		// set slope detection threshold
		as_dma_access(BMA250_SLOPE_TH, 0x14, DMA_SPI_WRITE);
		// map slope interrupt to INT1 pin
		as_dma_access(BMA250_IMR1, BMA250_SLOPE_INT1, DMA_SPI_WRITE);
	} else if(mode == BMP_AS_MODE_TAP) {
		// enable tap detection interrupt
		as_dma_access(BMA250_ISR1, BMA250_S_TAP_EN, DMA_SPI_WRITE);
		// set tap detection threshold
		as_dma_access(BMA250_TAP_TH, 0x0A, DMA_SPI_WRITE);
		// map tap interrupt to INT1 pin
		as_dma_access(BMA250_IMR1, BMA250_S_TAP_INT1, DMA_SPI_WRITE);
	}
	// enable CC430 interrupt pin for data read out from acceleration sensor
	AS_INT_IFG &= ~AS_INT_PIN;                   // Reset flag
	AS_INT_IE  |=  AS_INT_PIN;                   // Enable interrupt
}

// *************************************************************************************************
// @fn          bmp_as_stop
// @brief       Power down acceleration sensor
// @param       none
// @return      none
// *************************************************************************************************
void bmp_as_stop(void)
{
    // Disable interrupt
	AS_INT_IE &= ~AS_INT_PIN;                    // Disable interrupt

	as_stop();
}

// *************************************************************************************************
// @fn          bmp_as_get_data
// @brief       Service routine to read acceleration values.
// @param       u16 *data		pointer to data array
// @return      none
// *************************************************************************************************
void bmp_as_get_data(u16 * data)
{
	// Dummy read LSB from LSB acceleration data to update MSB (BMA250 datasheet 4.4.1)
	u8 acceldata[6];
	u8 i;

	for(i=0;i<6;i++) {
		acceldata[i] = as_dma_access(BMA250_ACC_X_LSB + i, 0, DMA_SPI_READ);	//bulk read
	}

  	// Store X/Z/Y MSB acceleration data in buffer
#ifdef BMA_250_8BIT_RESULT
		*(data) = acceldata[1];
		*(data + 2) = acceldata[3];
		*(data + 1) = acceldata[5];
#else
		*(data) = acceldata[0] >> 6 | acceldata[1] << 2;
		*(data + 2) = acceldata[2] >> 6 | acceldata[3] << 2;
		*(data + 1) = acceldata[4] >> 6 | acceldata[5] << 2;
#endif

}
