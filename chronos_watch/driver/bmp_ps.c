// *************************************************************************************************
//
//      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
//
//
//        Redistribution and use in source and binary forms, with or without
//        modification, are permitted provided that the following conditions
//        are met:
//
//          Redistributions of source code must retain the above copyright
//          notice, this list of conditions and the following disclaimer.
//
//          Redistributions in binary form must reproduce the above copyright
//          notice, this list of conditions and the following disclaimer in the
//          documentation and/or other materials provided with the
//          distribution.
//
//          Neither the name of Texas Instruments Incorporated nor the names of
//          its contributors may be used to endorse or promote products derived
//          from this software without specific prior written permission.
//
//        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// *************************************************************************************************
// Bosch BMP085 pressure sensor driver functions
// *************************************************************************************************

// *************************************************************************************************
// Include section

// system
#include "project.h"

// driver
#include "bmp_ps.h"
#include "i2c.h"
#include "bmp085.h"

// *************************************************************************************************
// Prototypes section

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section
// Variable to hold BMP085 calibration data
static bmp_085_calibration_param_t bmp_cal_param;


// *************************************************************************************************
// @fn          bmp_ps_get_cal_param
// @brief       read out calibration parameters from BMP085 memory to cal_param
// @param       none
// @return      none
// *************************************************************************************************
void bmp_ps_get_cal_param(void)
{
  /*parameters AC1-AC6*/
  bmp_cal_param.ac1 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 0, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.ac2 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 2, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.ac3 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 4, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.ac4 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 6, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.ac5 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 8, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.ac6 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 10,PS_I2C_16BIT_ACCESS);
  
  /*parameters B1,B2*/
  bmp_cal_param.b1 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 12, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.b2 = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 14, PS_I2C_16BIT_ACCESS);
  
  /*parameters MB,MC,MD*/
  bmp_cal_param.mb = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 16, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.mc = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 18, PS_I2C_16BIT_ACCESS);
  bmp_cal_param.md = ps_read_register(BMP_085_I2C_ADDR, BMP_085_PROM_START_ADDR + 20, PS_I2C_16BIT_ACCESS);
}

// *************************************************************************************************
// @fn          bmp_ps_measure
// @brief       Init pressure sensor registers and start sampling
// @param       bmp_085_result_t *result		pointer to the result structure
// @return      none
// *************************************************************************************************
void bmp_ps_measure(bmp_085_result_t *result)
{
    u16 ut;
    u32 up;
	// Enable EOC IRQ on rising edge
	PS_INT_IFG &= ~PS_INT_PIN;
	PS_INT_IE |= PS_INT_PIN;

	// Start sampling temperature
    ps_write_register(BMP_085_I2C_ADDR, BMP_085_CTRL_MEAS_REG, BMP_085_T_MEASURE);
    //wait for measure complete
    u8 loop = 1;
    while(loop) {
    	// When idle go to LPM3
    	_BIS_SR(LPM3_bits + GIE);

    	if(sysflags.flag.ps_ready) {
    		sysflags.flag.ps_ready = 0;
    		loop = 0;
    	}
    }
    // Get temp bits from ADC_OUT registers
    ut = ps_read_register(BMP_085_I2C_ADDR, BMP_085_ADC_OUT_MSB_REG, PS_I2C_16BIT_ACCESS);


    // Start sampling data in standard mode
    ps_write_register(BMP_085_I2C_ADDR, BMP_085_CTRL_MEAS_REG, BMP_085_P_MEASURE + (BMP_085_P_OSS << 6));
    loop = 1;
    while(loop) {
		// When idle go to LPM3
		_BIS_SR(LPM3_bits + GIE);

		if(sysflags.flag.ps_ready) {
			sysflags.flag.ps_ready = 0;
			loop = 0;
		}
	}
    // Get MSB/LSB from ADC_OUT_MSB_REG
 #if BMP_085_P_OSS
    u8 up_xlsb;
    up = ps_read_register(BMP_085_I2C_ADDR, BMP_085_ADC_OUT_MSB_REG, PS_I2C_16BIT_ACCESS);
    up_xlsb = ps_read_register(BMP_085_I2C_ADDR, BMP_085_ADC_OUT_XLSB_REG, PS_I2C_8BIT_ACCESS);
    // assume BMP_085_P_OSS = 2
    up = (up * 4) + (up_xlsb >> 6);
 #else
    up = ps_read_register(BMP_085_I2C_ADDR, BMP_085_ADC_OUT_MSB_REG, PS_I2C_16BIT_ACCESS);
 #endif

    // Disable DRDY IRQ
    PS_INT_IE &= ~PS_INT_PIN;
    PS_INT_IFG &= ~PS_INT_PIN;

    //calculate result
    bmp_ps_get_result(ut, up, result);

}

// *************************************************************************************************
// @fn          bmp_ps_get_result
// @brief       Read out temperature and pressure. Format is Pa. Range is 30000 .. 120000 Pa.
// @param       u16 ut			//ut from sensor
//				u16 up			//up from sensor
// @return      u16                     13-bit temperature value in xx.x C format
// @return      u32                     15-bit pressure sensor value (Pa)
// *************************************************************************************************
void bmp_ps_get_result(u16 ut, u32 up, bmp_085_result_t *result)
{
    s32 pres, x1, x2, x3, b3, b6;
    long bmp_param_b5;
   	u32 b4, b7;

    // Add Compensation and convert decimal value to 0.1 C
	x1 = (((long) ut - (long) bmp_cal_param.ac6) * (long) bmp_cal_param.ac5) / 32768;
    x2 = ((long) bmp_cal_param.mc * 2048) / (x1 + bmp_cal_param.md);
    bmp_param_b5 = x1 + x2;

    result->temperature = ((bmp_param_b5 + 8) / 16);  // temperature in 0.1C

    // Add Compensation and convert decimal value to Pa
  
   b6 = bmp_param_b5 - 4000;
   //*****calculate B3************
   x1 = (b6 * b6) >> 12;	 	 
   x1 = (bmp_cal_param.b2 * x1) / 2048;

   x2 = (bmp_cal_param.ac2 * b6) / 2048;

   x3 = x1 + x2;

   b3 = (((((long) bmp_cal_param.ac1) * 4 + x3) << BMP_085_P_OSS) + 2) / 4;

   //*****calculate B4************
   x1 = (bmp_cal_param.ac3 * b6) / 8192;
   x2 = (bmp_cal_param.b1 * ((b6 * b6) >> 12)) / 65536;
   x3 = ((x1 + x2) + 2) / 4;
   b4 = (bmp_cal_param.ac4 * (u32) (x3 + 32768)) / 32768;
     
   b7 = ((up - b3) * (50000 >> BMP_085_P_OSS));
   if (b7 < 0x80000000)
   {
     pres = (b7 * 2) / b4;
   }
   else
   { 
     pres = (b7 / b4) * 2;
   }
   
   x1 = pres / 256;
   x1 *= x1;
   x1 = (x1 * BMP_SMD500_PARAM_MG) / 65536;
   x2 = (pres * BMP_SMD500_PARAM_MH) / 65536;
   result->pressure = pres + (x1 + x2 + BMP_SMD500_PARAM_MI) / 16;	// pressure in Pa

   return;
}

// *************************************************************************************************
// @fn          conv_pa_to_centimeter
// @brief       Convert pressure (Pa) to altitude (cm) using standard fomula.
// @param       s32 delta_p_meas              Standard Sea Level Pressure (Pa) - Pressure (Pa)
// @return      s32                     Altitude (cm)
// The equation: h = 8.3340 * x - 0.3414
// *************************************************************************************************
s32 conv_pa_to_centimeter(s32 delta_p_meas)
{
	s32 h = 8 * delta_p_meas;
    return (h);
}
