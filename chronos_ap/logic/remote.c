/*
 * remote.c
 *
 *  Created on: 2014骞�2鏈�4鏃�
 *      Author: limiao
 */

#include "driverlib.h"
#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"
#include "USB_API/USB_HID_API/UsbHid.h"
#include "USB_app/usbConstructs.h"

#include <string.h>
#include "project.h"
#include "remote.h"
#include "ta.h"
#include "ccSPI.h"
#include "statedef.h"
#include "smartrf_CC1101.h"


u8 process_rfcommand(void)
{
	u8 ret = 0;
	switch(radio_state) {
	case RADIO_IDLE:
		//do nothing
		break;
	case RADIO_TX_PENDING:
		led_flash();
		spi_strobe(CC1101_STROBE_STX, DMA_SPI_WRITE);
		Timer0_encode_init();
		radio_state = RADIO_TX_TRANSMITTING;
		break;
	case RADIO_TX_TRANSMITTING:
		break;
	case RADIO_TX_FINISHED:
		spi_strobe(CC1101_STROBE_SIDLE, DMA_SPI_WRITE);
		Timer0_encode_exit();	//stop timer
		radio_state = RADIO_IDLE;
		break;
	case RADIO_RX_PENDING:
		//start RX
		spi_strobe(CC1101_STROBE_SRX, DMA_SPI_READ);
		radio_state = RADIO_RX_RECEIVING;
		//init timer
		Timer0_decode_init();
		break;
	case RADIO_RX_RECEIVING:
		break;
	case RADIO_RX_FINISHED:
		//stop RX
		spi_strobe(CC1101_STROBE_SIDLE, DMA_SPI_WRITE);
		Timer0_decode_exit();	//stop timer
		radio_state = RADIO_IDLE;
		break;
	case RADIO_NOT_READY:
		// This means radio came from normal operation mode. init first.
		//set to OOK operation mode
		spi_dma_transfer((unsigned int)preferredOOKSettings, CC1101_PARAM_LENGTH);
		while(sysflags.flag.dma_finishd == 0) {
			_BIS_SR(LPM0_bits + GIE);
			__no_operation();                       // Required for debugger
		}
		sysflags.flag.dma_finishd = 0;

		spi_dma_transfer((unsigned int)ook_patable, CC1101_PATABLE_LENGTH);
		while(sysflags.flag.dma_finishd == 0) {
			__no_operation();                       // This is very short time. Not go to LPM
		}
		sysflags.flag.dma_finishd = 0;

		//set state
		radio_state = RADIO_IDLE;
		break;
	default:
		;
	}
	return ret;
}


u8 radio_capture_pulse(void)
{
	//process usb command
	if(sysflags.flag.usb0_data_received) {
		sysflags.flag.usb0_data_received = 0;
		//switch commands
		switch(databuffer.buffer.usb0_rx_buffer[1]) {
			//These are ook commands
		case USB_COMM_RF1101_CAPTURE:
			radio_state = RADIO_RX_PENDING;
			break;
		case USB_COMM_CC1101_IDLE:
			radio_state = RADIO_RX_FINISHED;
		default:
			;
		}
		return 0;
	}

	//USB1
	if(sysflags.flag.usb1_data_received) {
		sysflags.flag.usb1_data_received = 0;
		if(databuffer.buffer.usb1_rx_buffer[1] >= 50) {
			radio_state = RADIO_TX_PENDING;
		}
		return 0;
	}

	//tx finish
	if(sysflags.flag.cc1101_tx_finished) {
		sysflags.flag.cc1101_tx_finished = 0;
		radio_state = RADIO_TX_FINISHED;
		//send return code
		databuffer.buffer.usb0_tx_buffer[0] = 0;
		hidSendDataInBackground(databuffer.buffer.usb0_tx_buffer, 1, HID0_INTFNUM, 1);
		return 0;
	}

	//rx has data
	if(sysflags.flag.cc1101_pulse_captured) {
		sysflags.flag.cc1101_pulse_captured = 0;
		//low to high
		pulse_seg[0] = TA0CCR1;		//high level time
		pulse_seg[1] = TA0CCR2;	//low level time
		TA0CCR1 = 0;
		TA0CCR2 = 0;
		if(pulse_seg[1] > 0x30) {
			led_flash();
			hidSendDataInBackground(databuffer.buffer.spi_rx_buffer, datapos, HID1_INTFNUM, 1);
			datapos = 0;
			datasync = 1;
		}
		if(datasync) {
			databuffer.buffer.spi_rx_buffer[datapos] = pulse_seg[0];
			databuffer.buffer.spi_rx_buffer[datapos + 1] = pulse_seg[1];
			if(datapos < 60) {
				datapos+=2;
			}
		}
	}
	return 1;
}
