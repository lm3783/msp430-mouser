/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

// *************************************************************************************************
//  USB control.
// *************************************************************************************************


// *************************************************************************************************
// Include section

#include "driverlib.h"
#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"
#include "USB_API/USB_HID_API/UsbHid.h"
#include "USB_app/usbConstructs.h"

#include "usbstate.h"
#include "smartrf_CC1101.h"
#include "ccSPI.h"
#include "statedef.h"

// *************************************************************************************************
// @fn          usb_checkstate
// @brief       Check USB state. If usb0 is active, process command; If usb1 is active, receive to buffer
// @param       none
// @return      none
// *************************************************************************************************
void usb_checkstate(void)
{
	u8 send = 1;	// whether to send return value to endpoint
	u8 count = 0;	// tx/rx count
	// This switch() creates separate main loops, depending on whether USB
	// is enumerated and active on the host, or disconnected/suspended.  If
	// you prefer, you can eliminate the switch, and just call
	// USB_connectionState() prior to sending data (to ensure the state is
	// ST_ENUM_ACTIVE).
	switch (USB_connectionState()) {
	// This case is executed while your device is connected to the USB
	// host, enumerated, and communication is active.  Never enter
	// LPM3/4/5 in this mode; the MCU must be active or LPM0 during USB
	// communication.
	case ST_ENUM_ACTIVE:
		// Enter LPM0 until awakened by an event handler
		//__bis_SR_register(LPM0_bits + GIE);
		//__no_operation();

		// Exit LPM because of a data-receive event, & fetch rx'ed data
		// HID0 is the command interface.
		if (sysflags.flag.usb0_event) { // HID0
			sysflags.flag.usb0_event = 0;
			count = hidReceiveDataInBuffer(databuffer.buffer.usb0_rx_buffer + 1, USB_BUFFER_SIZE, HID0_INTFNUM);
			databuffer.buffer.usb0_rx_buffer[0] = count;

			//switch commands
			switch(databuffer.buffer.usb0_rx_buffer[1]) {
			// These are global commands.
			case USB_COMM_NOP:
				databuffer.buffer.usb0_tx_buffer[0] = 0;
				break;
			case USB_COMM_GET_VERSION:
				databuffer.buffer.usb0_tx_buffer[0] = 1;
				break;
			case USB_COMM_GET_CURRENT_MODE:
				databuffer.buffer.usb0_tx_buffer[0] = operation_mode;
				break;
			case USB_COMM_SET_CURRENT_MODE:
				operation_mode = databuffer.buffer.usb0_rx_buffer[2];
				databuffer.buffer.usb0_tx_buffer[0] = operation_mode;
				//stop radio operation is done by each mode
				P2IE = 0;	//clear ifg
				sysflags.flag.cc1101_pulse_captured = 0;
				sysflags.flag.cc1101_rx_finished = 0;
				sysflags.flag.cc1101_tx_finished = 0;
				sysflags.flag.cc1101_tx_pending = 0;
				radio_state = RADIO_NOT_READY;
				break;

			//These are debug commands.
			case USB_COMM_CC1101_STROBE:
				if(count == 3) {
					databuffer.buffer.usb0_tx_buffer[0] = spi_strobe(databuffer.buffer.usb0_rx_buffer[2], databuffer.buffer.usb0_rx_buffer[3]);
				} else {
					send = 0;
				}
				break;
			case USB_COMM_CC1101_REGISTER:
				if(count == 4) {
					databuffer.buffer.usb0_tx_buffer[0] = spi_register(databuffer.buffer.usb0_rx_buffer[2], databuffer.buffer.usb0_rx_buffer[3], databuffer.buffer.usb0_rx_buffer[4]);
				} else {
					send = 0;
				}
				break;

			//These are mode-specific commands.
			default:
				sysflags.flag.usb0_data_received = 1;
				send = 0;
			}
			//send return code
			if(send) {
				hidSendDataInBackground(databuffer.buffer.usb0_tx_buffer, 1, HID0_INTFNUM, 1);
			}
		}

		// HID1 is meant to transfer data to CC1101. But the transfer is controlled by current mode.
		if (sysflags.flag.usb1_event) { // HID1
			sysflags.flag.usb1_event = 0;
			count = hidReceiveDataInBuffer(databuffer.buffer.usb1_rx_buffer + 2, USB_BUFFER_SIZE, HID1_INTFNUM);
			// endpoint 1 data will go to FIFO sooner or later. Set address first.
			databuffer.buffer.usb1_rx_buffer[0] = CC1101_TX_FIFO;
			databuffer.buffer.usb1_rx_buffer[1] = count;
			sysflags.flag.usb1_data_received = 1;
		}
		break;

		// These cases are executed while your device is:
	case ST_USB_DISCONNECTED: // physically disconnected from the host
	case ST_ENUM_SUSPENDED:   // connected/enumerated, but suspended
	case ST_NOENUM_SUSPENDED: // connected, enum started, but the host is unresponsive

		// In this example, for all of these states we enter LPM3.  If
		// the host performs a "USB resume" from suspend, the CPU will
		// automatically wake.  Other events can also wake the
		// CPU, if their event handlers in eventHandlers.c are
		// configured to return TRUE.
		// __bis_SR_register(LPM3_bits + GIE);
		break;

		// The default is executed for the momentary state
		// ST_ENUM_IN_PROGRESS.  Almost always, this state lasts no more than a
		// few seconds.  Be sure not to enter LPM3 in this state; USB
		// communication is taking place, so mode must be LPM0 or active.
	case ST_ENUM_IN_PROGRESS:
	default:
		;
	}
	return;
}
