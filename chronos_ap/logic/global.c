// *************************************************************************************************
// Include section

#include "driverlib.h"
#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"
#include "USB_API/USB_HID_API/UsbHid.h"
#include "USB_app/usbConstructs.h"

#include "global.h"
#include "ccxx00.h"
#include "statedef.h"
#include "smartrf_CC1101.h"
#include "keyboard.h"
#include "ta.h"
#include "ccSPI.h"
#include "statedef.h"
#include "keyboard.h"

// *************************************************************************************************
// @fn          process_command
// @brief       Process command sent from chronos.
// @param       none
// @return      0 means radio process is complete, program will continue;1 means radio process is on the way, this function will be rerun.
// *************************************************************************************************
u8 process_command(void)
{
	u8 ret = 1;

	//process usb command
	if(sysflags.flag.usb0_data_received) {
		sysflags.flag.usb0_data_received = 0;
		//switch commands
		switch(databuffer.buffer.usb0_rx_buffer[1]) {
		case USB_COMM_CC1101_IDLE:
			radio_state = RADIO_IDLE_PENDING;
			break;
		case USB_COMM_KEYBOARD:
			Keyboard_sendReport(databuffer.buffer.usb0_rx_buffer[2]);
			break;
		case USB_COMM_CC1101_TX:
			radio_state = RADIO_TX_PENDING;
			break;
		case USB_COMM_CC1101_RX:
			radio_state = RADIO_RX_PENDING;
			break;
		}
		return 0;
	}

	//RX finish, process command
	if(sysflags.flag.cc1101_rx_finished) {
		sysflags.flag.cc1101_rx_finished = 0;
		//process each command
		switch(databuffer.buffer.spi_rx_buffer[2]) {
		case RADIO_OPERATION_RSSI:
			if(databuffer.buffer.spi_rx_buffer[3] < 3) {
				hidSendDataInBackground(databuffer.buffer.spi_rx_buffer + 1, databuffer.buffer.spi_rx_buffer[0], HID1_INTFNUM, 1);
			} else if(databuffer.buffer.spi_rx_buffer[3] == 3) {
				//data is stored in USB1 rx buffer, because SPI tx buffer may be overwritten by RX operation.
				databuffer.buffer.usb1_rx_buffer[0] = CC1101_TX_FIFO;
				databuffer.buffer.usb1_rx_buffer[1] = 2;	// length
				databuffer.buffer.usb1_rx_buffer[2] = RADIO_OPERATION_RSSI;	// command
				databuffer.buffer.usb1_rx_buffer[3] = 1;		//type
				radio_tx_state = RADIO_TX_INIT;
			} else if(databuffer.buffer.spi_rx_buffer[3] == 4) {
				radio_tx_state = RADIO_TX_EXIT;
			}
			break;
		case RADIO_OPERATION_PPT:
			Keyboard_sendReport(databuffer.buffer.spi_rx_buffer[3]);
			break;
		case RADIO_OPERATION_BICOM:
			if(databuffer.buffer.spi_rx_buffer[2] == 0) {
				radio_tx_state = RADIO_TX_EXIT;
			} else if(databuffer.buffer.spi_rx_buffer[2] == 1) {
				radio_tx_state = RADIO_TX_EXIT;
				// Just send data to PC. PC will do further process and send response if needed.
				hidSendDataInBackground(databuffer.buffer.spi_rx_buffer + 1, databuffer.buffer.spi_rx_buffer[0], HID1_INTFNUM, 1);
			}
			break;
		case RADIO_OPERATION_NOP:
		default:
			;
		}
		ret = 0;
	}

	//usb1 data
	if(sysflags.flag.usb1_data_received) {
		sysflags.flag.usb1_data_received = 0;
		radio_tx_state = RADIO_TX_INIT;
	}
	//TX phase
	if(radio_tx_state == RADIO_TX_INIT) {
		//trigger tx switch
		sysflags.flag.cc1101_tx_pending = 1;
		Timer2_A1_Delay(32767, 0);	//start timer
		radio_tx_state = RADIO_TX_NOP;
	} else if(radio_tx_state == RADIO_TX_EXIT) {
		//stop timer
		Timer2_A1_Stop();
		//unset bit
		sysflags.flag.cc1101_tx_pending = 0;
		//uninit
		radio_tx_state = RADIO_TX_NOP;
	}
	//Switch phase
	if(sysflags.flag.ta_timeup) {
		sysflags.flag.ta_timeup = 0;
		if(sysflags.flag.cc1101_tx_pending) {
			sysflags.flag.cc1101_tx_pending = 0;
		} else {
			sysflags.flag.cc1101_tx_pending = 1;
		}
		Timer2_A1_Delay(32767, 0);	//reset timer
		ret = 0;
	}

	//TX finish.
	if(sysflags.flag.cc1101_tx_finished) {
		sysflags.flag.cc1101_tx_finished = 0;
		if(sysflags.flag.cc1101_tx_pending) {
			radio_state = RADIO_TX_PENDING;
		} else {
			radio_state = RADIO_RX_PENDING;
		}
		ret = 0;
	}

	return ret;
}

// *************************************************************************************************
// @fn          radio_checkstate
// @brief       Check radio state. Radio is always in RX mode. Program needs to manually trigger TX if needed.
//				In TX mode, radio is in 500ms TX - 500ms RX repeat. Chronos is in 500ms WOR mode.
//					When chronos receive TX data, it will wait 500ms and send ACK. So ACK is always in AP RX phase.
//					When TX is finished, radio will automatically switch to RX mode.
// @param       none
// @return      0 means radio process is complete, program will continue;1 means radio process is on the way, this function will be rerun.
// *************************************************************************************************
u8 radio_checkstate(void)
{
	u8 ret = 0;
	u8 length;
	switch(radio_state) {
	case RADIO_IDLE_PENDING:
		P2IE = 0;	//clear ifg
		spi_strobe(CC1101_STROBE_SIDLE, DMA_SPI_WRITE);
		spi_strobe(CC1101_STROBE_SFTX, DMA_SPI_WRITE);
		spi_strobe(CC1101_STROBE_SFRX, DMA_SPI_READ);
		radio_state = RADIO_IDLE;
		ret = 1;
		break;
	case RADIO_IDLE:
		//do nothing
		break;
	case RADIO_TX_PENDING:
		spi_dma_transfer((unsigned int) databuffer.buffer.usb1_rx_buffer, databuffer.buffer.usb1_rx_buffer[1] + 2);
		radio_state = RADIO_TX_FILLING;
		ret = 1;
		break;
	case RADIO_TX_FILLING:
		break;
	case RADIO_TX_FILLED:
		led_flash();
		spi_strobe(CC1101_STROBE_STX, DMA_SPI_WRITE);
		radio_state = RADIO_TX_TRANSMITTING;
		P2IE = 1;
		ret = 1;
		break;
	case RADIO_TX_TRANSMITTING:
		break;
	case RADIO_TX_FINISHED:
		radio_state = RADIO_IDLE;	//state to idle, but cc1101 is in sftxon
		sysflags.flag.cc1101_tx_finished = 1;
		break;
	case RADIO_RX_PENDING:
		spi_strobe(CC1101_STROBE_SRX, DMA_SPI_READ);
		radio_state = RADIO_RX_RECEIVING;
		P2IE = 1;
		ret = 1;
		break;
	case RADIO_RX_RECEIVING:
		if(sysflags.flag.cc1101_tx_pending) {
			// there is an tx waiting
			P2IE = 0;	//clear ifg
			radio_state = RADIO_TX_PENDING;
			ret = 1;
		} else {
			length = spi_strobe(CC1101_STROBE_SNOP, DMA_SPI_READ);
			if((length >> 4) == 6) {		//rx overflow
				led_flash();
				spi_strobe(CC1101_STROBE_SFRX, DMA_SPI_READ);
				radio_state = RADIO_RX_PENDING;
				ret = 1;
			}
		}
		break;
	case RADIO_RX_FINISHED:
		led_flash();
		length = spi_register(CC1101_RXBYTES, 0, DMA_SPI_READ);
		databuffer.buffer.spi_tx_buffer[0] = CC1101_RX_FIFO;
		spi_dma_transfer((unsigned int)databuffer.buffer.spi_tx_buffer, length + 1);
		radio_state = RADIO_RX_FILLING;
		ret = 1;
		break;
	case RADIO_RX_FILLING:
		break;
	case RADIO_RX_FILLED:
		length = databuffer.buffer.spi_rx_buffer[0];
		// total length == data length + length byte + RSSI byte + LQI byte
		// LQI byte has CRC bit
		if((length == databuffer.buffer.spi_rx_buffer[1] + 3) && (databuffer.buffer.spi_rx_buffer[length] & BIT7)) {
			sysflags.flag.cc1101_rx_finished = 1;
		}
		radio_state = RADIO_RX_PENDING;	//radio is always in RX mode
		break;
	case RADIO_NOT_READY:
		//This means radio finished remote function. Go to normal operation mode.
		spi_dma_transfer((unsigned int)preferredSettings, CC1101_PARAM_LENGTH);
		while(sysflags.flag.dma_finishd == 0) {
			_BIS_SR(LPM0_bits + GIE);
			__no_operation();                       // Required for debugger
		}
		sysflags.flag.dma_finishd = 0;

		spi_dma_transfer((unsigned int)patable, CC1101_PATABLE_LENGTH);
		while(sysflags.flag.dma_finishd == 0) {
			__no_operation();                       // This is very short time. Not go to LPM
		}
		sysflags.flag.dma_finishd = 0;

		radio_state = RADIO_RX_PENDING;	//radio is always in RX mode
		ret = 1;
		break;
	default:
		;
	}
	return ret;
}
