/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/*  
 * ======== main.c ========
 */



#include "driverlib.h"
#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"
#include "USB_API/USB_HID_API/UsbHid.h"
#include "USB_app/usbConstructs.h"

#include "project.h"
#include "hal.h"
#include "usbstate.h"
#include "global.h"
#include "remote.h"
#include "ta.h"

//Radio state.
volatile u8 radio_state = 0xFF;		//uninit
volatile u8 operation_mode = 0;

//Definiation on respective HID0/1 interfaces
volatile s_system_flags sysflags;

// data buffer for spi and usb
bufferUnion databuffer;
//usb2 is the keyboard, which only transmits
KeyUnion usb2_tx_buffer;

void (*current_program)(void);

// ========================main=====================
void main(void) {
	// Set up clocks/IOs.  initPorts()/initClocks() will need to be customized
	// for your application, but MCLK should be between 4-25MHz.  Using the
	// DCO/FLL for MCLK is recommended, instead of the crystal.  For examples
	// of these functions, see the complete USB code examples.  Also see the
	// Programmer's Guide for discussion on clocks/power.
	WDTCTL = WDTPW + WDTHOLD;	// Stop watchdog timer

	// Minimum Vcore setting required for the USB API is PMM_CORE_LEVEL_2 .
	PMM_setVCore(PMM_CORE_LEVEL_3);

	initPorts();             // Configure all GPIOs
	initClocks();     // Configure clocks
	initXT2();			// Configure XT2 clock signal
	USB_setup(TRUE, TRUE);   // Init USB & events; if a host is present, connect
	__enable_interrupt();    // Enable interrupts globally

	u8 ret;
	while (1) {
		//check USB state. return operation mode.
		usb_checkstate();
		switch(operation_mode) {
		case 0:
			//Timer2_A1_Delay(32768 - 1, 1);
			operation_mode = 1;
			break;
		case 1:
			//check radio state.
			while(radio_checkstate());
			//process global command. Used to switch between different operation modes.
			ret = process_command();
			break;
		case 2:
			while(process_rfcommand());
			ret = radio_capture_pulse();
			break;
		default:
			ret = 0;
		}
		//Enter LPM
		if(ret) {
			__bis_SR_register(LPM0_bits + GIE);
			__no_operation();
		}
	}  //while(1)
} //main()

/*  
 * ======== UNMI_ISR ========
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR(void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(UNMI_VECTOR))) UNMI_ISR (void)
#else
#error Compiler not found!
#endif
{
	switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG)) {
	case SYSUNIV_NONE:
		__no_operation();
		break;
	case SYSUNIV_NMIIFG:
		__no_operation();
		break;
	case SYSUNIV_OFIFG:
#ifndef DRIVERLIB_LEGACY_MODE
		UCS_clearAllOscFlagsWithTimeout(0);
		SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
#else
		UCS_clearAllOscFlagsWithTimeout(UCS_BASE, 0);
		SFR_clearInterrupt(SFR_BASE, SFR_OSCILLATOR_FAULT_INTERRUPT);

#endif
		break;
	case SYSUNIV_ACCVIFG:
		__no_operation();
		break;
	case SYSUNIV_BUSIFG:
		// If the CPU accesses USB memory while the USB module is
		// suspended, a "bus error" can occur.  This generates an NMI, and
		// execution enters this case.  This should never occur.  If USB is
		// automatically disconnecting in your software, set a breakpoint
		// here and see if execution hits it.  See the Programmer's
		// Guide for more information.
		SYSBERRIV = 0; // Clear bus error flag
		USB_disable(); // Disable USB -- USB must be reset after a bus error
	}
}
//Released_Version_4_10_02
