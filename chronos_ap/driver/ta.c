// *************************************************************************************************
//  Hardware Timer Driver
//  Function: TA2: delay function; TA1: LED flash; TA0: radio signal decode
// *************************************************************************************************

// system
#include <string.h>
#include "project.h"
#include "ta.h"

void Timer0_decode_init(void)
{
    TA0CCR0 = 32768 - 1;
    TA0CTL = TASSEL__ACLK | MC__UP | TACLR;	//start timer
    TA0CCTL4 = (CM1 | CM0 | SCS | CAP | CCIE);	//Capture on raise, enable interrupt
}

void Timer0_decode_exit(void)
{
	TA0CTL &= ~MC__UP;	//stop timer
	TA0CCTL4 &= ~CCIE;	//disable interrupt
}

void Timer0_encode_init(void)
{
	P1DIR |= BIT5;		//set pin to output mode
	TA0CCTL4 = OUT;
	TA0CCTL4 |= OUTMOD_5;	//OUTMOD 7 cannot be used because of TA0CCR1 ISR. Need to manually reset.
	TA0CCTL0 = CCIE;
	TA0CCR4 = 10;		//sync bit
	TA0CCR0 = 320;		//sync total
	TA0CCR1 = 4;		//position(4 is the first data byte)
	TA0CCR2 = 0;		//repeat times
	TA0CTL = TASSEL__ACLK | MC__UP | TACLR;	//start timer
}

void Timer0_encode_exit(void)
{
	TA0CCTL4 = 0;	//clear IE
	P1DIR &= ~BIT5;		//set pin to input mode
	TA0CCTL0 = 0;	//clear IE
}

// *************************************************************************************************
// @fn          led_flash
// @brief       flash led for 100 ms
// @param       none
// @return      none
// *************************************************************************************************
void led_flash(void)
{
    // ---------------------------------------------------------------------
    // Configure Timer1 for use by the clock and delay functions
    // Set interrupt frequency to 1Hz
    TA1CCR0 = 32768 - 1;

    TA1CCR1 = 64;	//20ms
    TA1CCTL1 |= CCIE;	//enable interrupt

    TA1CTL |= TASSEL__ACLK | MC__UP | TACLR;	//start timer
    LED_ON;
}

// *************************************************************************************************
// @fn          Timer2_A1_Delay
// @brief       Wait for some microseconds
// @param       ticks (1 tick = 1/32768 sec)
//				latched (wait unitl timeout)
// @return      none
// *************************************************************************************************
void Timer2_A1_Delay(u16 ticks, u8 latched)
{
    // ---------------------------------------------------------------------
    // Configure Timer1 for use by the clock and delay functions
    // Set interrupt frequency to 1Hz
    TA2CCR0 = 32768 - 1;

    TA2CCR1 = ticks;	//set dest
    TA2CCTL1 |= CCIE;	//enable interrupt

    TA2CTL |= TASSEL__ACLK | MC__UP | TACLR;	//start timer

	u8 module_loop = 1;
    if(latched) {
		// loop stage
		while(module_loop) {
			// When idle go to LPM3
			_BIS_SR(LPM0_bits + GIE);

			if(sysflags.flag.ta_timeup) {
				sysflags.flag.ta_timeup = 0;
				module_loop = 0;
			}
		}
    } else {
    	sysflags.flag.ta_timeup = 0;
    }
}

// *************************************************************************************************
// @fn          Timer2_A1_Stop
// @brief       Stop Timer
// @param       none
// @return      none
// *************************************************************************************************
void Timer2_A1_Stop(void)
{
	//stop ta1 timer
	TA2CTL &= ~MC__UP;
	TA2CCTL1 &= ~CCIE;	//disable interrupt
	sysflags.flag.ta_timeup = 0;	//clear bit
}

// *************************************************************************************************
// @fn          TIMER2_A1_ISR
// @brief       IRQ handler for TIMER2_A1 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER2_A1_VECTOR
__interrupt void TIMER2_A1_3_ISR(void)
{
	u16 timeriv = TA2IV;
	if(timeriv == TA2IV_TACCR1) {
		sysflags.flag.ta_timeup = 1;
	    //stop ta1 timer
	    TA2CTL &= ~MC__UP;
	    TA2CCTL1 &= ~CCIE;	//disable interrupt
		_BIC_SR_IRQ(LPM3_bits);
	}
}

// *************************************************************************************************
// @fn          TIMER1_A1_ISR
// @brief       IRQ handler for TIMER1_A1 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER1_A1_VECTOR
__interrupt void TIMER1_A1_3_ISR(void)
{
	u16 timeriv = TA1IV;
	if(timeriv == TA1IV_TA1CCR1) {
	    TA1CTL &= ~MC__UP;
	    TA1CCTL1 &= ~CCIE;	//disable interrupt
		LED_OFF;
	}
}

// *************************************************************************************************
// @fn          TIMER0_A1_ISR
// @brief       IRQ handler for TIMER0_A1 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_5_ISR(void)
{
	u16 timeriv = TA0IV;
	if(timeriv == TA0IV_TA0CCR4) {
		if(TA0CCTL4 & CCI) {
			//down level
			TA0CCR2 = TA0CCR4;
			sysflags.flag.cc1101_pulse_captured = 1;
			TA0CTL |= TACLR;	//reset timer;
			__bic_SR_register_on_exit(LPM3_bits);   // Exit LPM0-3
			__no_operation();                      // Required for debugger
		} else {
			//up level
			TA0CCR1 = TA0CCR4;
		}
	}
}

// *************************************************************************************************
// @fn          TIMER0_A0_ISR
// @brief       IRQ handler for TIMER0_A0 IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
	u8 pos = TA0CCR1;
	if(pos <= 50) {	//24bit * 2 and skip 4 initial byte
		TA0CCR4 = databuffer.buffer.usb1_rx_buffer[pos];		//data
		TA0CCR0 = 40;	//always 40. If set according to data and it is 41, the counter won't reset
		TA0CCR1 = pos + 2;
	} else {
		if(TA0CCR2 < 4) {	//repeat 4 times
			//start another round
			TA0CTL |= TACLR;	//reset timer. If not it will count from 40, not 0
			TA0CCR4 = 10;
			TA0CCR0 = 320;
			TA0CCR1 = 4;
			TA0CCR2++;
		} else {
			//exit
			sysflags.flag.cc1101_tx_finished = 1;
			TA0CTL &= ~MC__UP;		//stop timer
			__bic_SR_register_on_exit(LPM3_bits);   // Exit LPM0-3
			__no_operation();                      // Required for debugger
		}
	}
	TA0CCTL4 = OUT;		//reset output. ISR prevents the automatic reset.
	TA0CCTL4 |= OUTMOD_5;
}
