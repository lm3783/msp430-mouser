/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/*
 * ======== hal.c ========
 *
 */

#include "hal.h"
#include "ccSPI.h"
#include "smartrf_CC1101.h"

/*
 * This function drives all the I/O's as output-low, to avoid floating inputs
 * (which cause extra power to be consumed).
 */
void initPorts(void) {
	// Initialize all unused pins as low level output
	P4OUT &= ~( BIT4 | BIT5 | BIT6 | BIT7);
	P5OUT &= ~(BIT0 | BIT1);
	P6OUT &= ~(BIT0 | BIT1 | BIT2 | BIT3);
	P4DIR |= ( BIT4 | BIT5 | BIT6 | BIT7);
	P5DIR |= (BIT0 | BIT1);
	P6DIR |= (BIT0 | BIT1 | BIT2 | BIT3);
	// These are debug pins.
	PJOUT &= ~(BIT0 | BIT1 | BIT2 | BIT3);
	PJDIR |= (BIT0 | BIT1 | BIT2 | BIT3);

	// LED drive pins.
	P1DIR |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4);
	// LED OFF
	P1OUT &= ~(BIT0 | BIT1 | BIT2 | BIT3 | BIT4);

	//set CC1101 interrupt pin
	P2IES = 1;	// trigger on high-to-low transition
	P2IFG = 0;	// clear IFG state

	P1SEL |= BIT5;	// configure P1.5 to TA0.4 input

	// CC1101 GDO0 to P1.5-P1.7 and P2.0, which are configured to input by default.
	// CC1101 SPI to P4.0-P4.3, which are configured as SPI by default.
}

/* Configures the system clocks:
 * MCLK = SMCLK = DCO/FLL = MCLK_FREQUENCY (16MHz)
 * ACLK = FLLref = REFO=32kHz
 *
 * XT2 is not configured here.  Instead, the USB API automatically starts XT2
 * when beginning USB communication, and optionally disables it during USB
 * suspend.
 */
void initClocks(void) {
	// Enable 32kHz ACLK on XT1 via external crystal and 26MHz on XT2 via external clock signal
	P5SEL |= (BIT4 | BIT5); // Select XINs and XOUTs
	//  UCSCTL6 &= ~XT1OFF;        // Switch on XT1, keep highest drive strength - default
	//  UCSCTL6 |=  XCAP_3;        // Set internal load caps to 12pF - default
	//  UCSCTL4 |=  SELA__XT1CLK;  // Select XT1 as ACLK - default

	// Configure clock system
	_BIS_SR(SCG0);// Disable FLL control loop
	UCSCTL0 = 0x0000; // Set lowest DCOx, MODx to avoid temporary overclocking
	// Select suitable DCO frequency range and keep modulation enabled
	UCSCTL1 = DCORSEL_5; // DCO frequency above 8MHz but not bigger than 16MHz
	UCSCTL2 = FLLD__2 | (((MCLK_FREQUENCY + 0x4000) / 0x8000) - 1); // Set FLL loop divider to 2 and
																	// required DCO multiplier
	//  UCSCTL3 |= SELREF__XT1CLK;                    // Select XT1 as FLL reference - default
	//  UCSCTL4 |= SELS__DCOCLKDIV | SELM__DCOCLKDIV; // Select XT1 as ACLK and
	// divided DCO for SMCLK and MCLK - default
	_BIC_SR(SCG0);// Enable FLL control loop again

	// Loop until XT1 and DCO fault flags are reset
	do {
		// Clear fault flags
		UCSCTL7 &= ~(XT2OFFG | XT1LFOFFG | DCOFFG);
		SFRIFG1 &= ~OFIFG;
	} while ((SFRIFG1 & OFIFG));

	// Worst-case settling time for the DCO when changing the DCO range bits is:
	// 32 x 32 x MCLK / ACLK
	__delay_cycles(((32 * MCLK_FREQUENCY) / 0x8000) * 32);

	_BIS_SR(SCG0);    // Disable FLL control loop
}

/* Setup CC1101 to output XT2 clock signal
 *
 */
void initXT2(void) {
	// Configure SPI
	initSPI();
	// Configure CC1101
	__enable_interrupt();
	spi_dma_transfer((unsigned int)preferredSettings, CC1101_PARAM_LENGTH);
	// wait for transfer finish since we need the clock
	while(sysflags.flag.dma_finishd == 0) {
		_BIS_SR(LPM0_bits + GIE);
		__no_operation();                       // Required for debugger
	}
	sysflags.flag.dma_finishd = 0;
	 //patable is not set here, it will be set when operation change to 1

	__disable_interrupt();
	// Receive XT2 cloxk
	UCSCTL6 |= XT2BYPASS;
}


//Released_Version_4_10_02
