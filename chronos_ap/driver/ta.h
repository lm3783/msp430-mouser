// *************************************************************************************************
//  Hardware Timer Driver
// *************************************************************************************************

#ifndef TA_H_
#define TA_H_

#define LED_OFF		{P1OUT &= ~(BIT0 | BIT1 | BIT2 | BIT3 | BIT4);}
#define LED_ON		{P1OUT |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4);}


void led_flash(void);
void Timer2_A1_Delay(u16 ticks, u8 latched);
void Timer2_A1_Stop(void);
void Timer0_decode_init(void);
void Timer0_decode_exit(void);
void Timer0_encode_init(void);
void Timer0_encode_exit(void);

#endif /* TA_H_ */
