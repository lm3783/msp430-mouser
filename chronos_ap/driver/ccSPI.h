// *************************************************************************************************
// Public header for hardware SPI to TI's CCxx00 radios.
// *************************************************************************************************

#ifndef __CC_SPI_H
#define __CC_SPI_H


// *************************************************************************************************
// Include section
#include "project.h"

// *************************************************************************************************
// Defines section

// Port and pin resources for SPI interface to acceleration sensor
// CSN pin to high
#define CC_CSN_HIGH                    {P4OUT |= (BIT0);}
// CSN pin to low
#define CC_CSN_LOW                     {P4OUT &= ~(BIT0);}
// SDI pin level read out
#define SDI_LEVEL                      (P4IN & BIT2)

//DMA channel
#define DMA_SPI_READ	0
#define DMA_SPI_WRITE	1
#define DMA_SPI_DATA_LENGTH		2

// *************************************************************************************************
// Typedef section

// *************************************************************************************************
// API section

void initSPI(void);
u8 spi_register(u8 address, u8 value, u8 mode);
u8 spi_strobe(u8 address, u8 mode);
void spi_dma_transfer(unsigned int address, unsigned int length);

// *************************************************************************************************
#endif // __CC_SPI_H
// End of file
