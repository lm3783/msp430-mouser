// *************************************************************************************************
//  Hardware SPI interface to TI's CCxx00 radios. Supports writing bytes and words.
// *************************************************************************************************


// *************************************************************************************************
// Include section

#include "ccSPI.h"
#include "smartrf_CC1101.h"
#include "statedef.h"

// *************************************************************************************************
// Defines section
u8 spi_strobe(u8 address, u8 mode)
{
	u8 result;

		if(mode == DMA_SPI_WRITE) {
			address &= ~BIT7;
		} else {
			address |= BIT7;
		}

		//init transfer
		CC_CSN_LOW;

		//set data
		UCB1TXBUF = address;

		while (UCB1STAT & UCBUSY);
	    result = UCB1RXBUF;
	    //cleanup
	    CC_CSN_HIGH;

	    return result;
}

u8 spi_register(u8 address, u8 value, u8 mode)
{
	u8 result;

		if(mode == DMA_SPI_WRITE) {
			address &= ~BIT7;
		} else {
			address |= BIT7;
			value = 0;
		}

		//init transfer
		CC_CSN_LOW;

		UCB1TXBUF = address;
		while (UCB1STAT & UCBUSY);
		UCB1TXBUF = value;
		while (UCB1STAT & UCBUSY);
	    result = UCB1RXBUF;

	    //cleanup
	    CC_CSN_HIGH;
	    return result;
}

/* Configure SPI and reset CC1101
 * The old code used manual reset, which is too complicated.
 * This version use automatic POR.
 */
void initSPI(void) {
	// Initialize SPI interface to radio
	UCB1CTL0 |= UCSYNC | UCMST | UCMSB // SPI master, 8 data bits,  MSB first,
			| UCCKPH;            //  clock idle low, data output on falling edge
	UCB1CTL1 |= UCSSEL1;               // SMCLK as clock source
	UCB1BR0 = CC_BR_DIVIDER;        // Low byte of division factor for baud rate
	UCB1BR1 = 0x00;                // High byte of division factor for baud rate
	UCB1CTL1 &= ~UCSWRST;              // Start SPI hardware

	CC_CSN_HIGH;	//set CSN to high at init
	P4DIR |= (BIT0 | BIT1 | BIT3);
	CC_CSN_LOW;
	while (SDI_LEVEL);	// Wait for SDI indicating chip ready
	CC_CSN_HIGH;	//finish POR

	// Pins to SPI function
	P4SEL |= (BIT1 | BIT2 | BIT3);

	// Send SRES strobe
	UCB1TXBUF = CC1101_STROBE_SRES;
	// Wait until XOSC and voltage regulator stabilized
	__delay_cycles(100ul * (MCLK_FREQUENCY / 1000000)); // 100us at selected CPU clock speed

	// Setup DMA channel
	//Init DMA engine
	//Channel 1 is used for Tx
	//Channel 2 is used for Rx
	DMACTL0 = DMA1TSEL_23;
	DMACTL1 = DMA2TSEL_22;

	//Tx: single transfer, source increase, byte mode
	DMA1CTL = DMASRCINCR_3 | DMADSTBYTE | DMASRCBYTE | DMALEVEL;
	//source address
	DMA1SAL = (unsigned int) databuffer.buffer.spi_tx_buffer;                    // Source block address
	DMA1DAL = (unsigned int) &UCB1TXBUF;                   // Destination single address
	//DMA1SZ = DMA_SPI_DATA_LENGTH;

	//Rx: repeat single transfer, destination increase, byte mode, interrupt enable
	DMA2CTL = DMADSTINCR_3 | DMADSTBYTE | DMASRCBYTE | DMALEVEL | DMAIE;
	//dest address
	DMA2SAL = (unsigned int) &UCB1RXBUF;                     // Source block address
	DMA2DAL = (unsigned int) databuffer.buffer.spi_rx_buffer;                      // Destination single address
	//DMA2SZ = DMA_SPI_DATA_LENGTH;

}

/* SPI DMA Transfer
 * This function returns immdetiatly.
 */
void spi_dma_transfer(unsigned int address, unsigned int length)
{
	DMA1SAL = address;

	DMA1SZ = length;
	DMA2SZ = length;
	//init transfer
	CC_CSN_LOW;

	// start transfer
    DMA1CTL |= DMAEN;
    DMA2CTL |= DMAEN;

}

// *************************************************************************************************
// @fn          DMA_ISR
// @brief       IRQ handler for DMA IRQ
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector = DMA_VECTOR
__interrupt void DMA_ISR(void)
{
	u16 dmaiv = DMAIV;	//clear IV and IFG
	if(dmaiv == 0x06) {
		//cleanup
		CC_CSN_HIGH;
		//set finish flag
		if(radio_state == RADIO_TX_FILLING) {
			radio_state = RADIO_TX_FILLED;
		} else if(radio_state == RADIO_RX_FILLING) {
			radio_state = RADIO_RX_FILLED;
		} else {
			sysflags.flag.dma_finishd = 1;
		}
		//wake up
        __bic_SR_register_on_exit(LPM3_bits);   // Exit LPM0-3
        __no_operation();                       // Required for debugger

	}

}

// *************************************************************************************************
// @fn          PORT2_ISR
// @brief       Interrupt service routine for
//                                      - buttons
//                                      - acceleration sensor INT1
//                                      - pressure sensor EOC
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector=PORT2_VECTOR
__interrupt void PORT2_ISR(void)
{
	u8 key_status = P2IV;
	if(radio_state == RADIO_TX_TRANSMITTING) {
		radio_state = RADIO_TX_FINISHED;
		P2IE = 0;
	    __bic_SR_register_on_exit(LPM3_bits);   // Exit LPM0-3
	    __no_operation();                       // Required for debugger

	} else if(radio_state == RADIO_RX_RECEIVING) {
		radio_state = RADIO_RX_FINISHED;
		P2IE = 0;
	    __bic_SR_register_on_exit(LPM3_bits);   // Exit LPM0-3
	    __no_operation();                       // Required for debugger
	}
}

// *************************************************************************************************
// End of file
