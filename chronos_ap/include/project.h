// *************************************************************************************************
// Global project defines, variables and functions
// *************************************************************************************************

#ifndef __PROJECT_H
#define __PROJECT_H

// *************************************************************************************************
// Include section

#include "msp430.h"
#include "bm.h"

// *************************************************************************************************
// Defines section

// MCLK frequency in Hz the MCU is running on (only used for timing calculations)
#define MCLK_FREQUENCY                 (16000000)

// DCO frequency division factor determining speed of the radio SPI interface in Hz
// Max. value for CC1101 is 6.5MHz
#define CC_BR_DIVIDER        ((MCLK_FREQUENCY / 6500000) + 1)

// Product ID
#define PRODUCT_ID                     (0x23456789)

// on respective HID0/1 interfaces
//USB buffer
#define USB_BUFFER_SIZE		65

#define USB_KEYARRAY_SIZE	8

// Static Variables
extern volatile u8 radio_state;	//radio state machine
extern volatile u8 operation_mode;

// Set of system flags
typedef union
{
    struct
    {
        u16 usb0_event : 1;
        u16 usb1_event : 1;
        u16 usb0_data_received : 1;
        u16 usb1_data_received : 1;
        u16 cc1101_tx_finished : 1;
        u16 cc1101_rx_finished : 1;
        u16 cc1101_tx_pending : 1;
        u16 cc1101_pulse_captured : 1;
        u16 ta_timeup : 1;					//TA timer timeup
        u16 dma_finishd : 1;				//This is used for init CC1101 parameters
    } flag;
    u16 all_flags;                        // Shortcut to all system flags (for reset)
} s_system_flags;
extern volatile s_system_flags sysflags;	//system flags

//	Low level key report: up to 6 keys and shift, ctrl etc at once
typedef union {
	u8 keyArray[USB_KEYARRAY_SIZE];
	struct
	{
		u8 modifiers;
		u8 reserved;
		u8 keys[6];
	} keyReport;
}KeyUnion;
extern KeyUnion usb2_tx_buffer;

typedef union {
	struct {
		u8 spi_rx_buffer[USB_BUFFER_SIZE];
		u8 spi_tx_buffer[USB_BUFFER_SIZE];
		u8 usb0_rx_buffer[USB_BUFFER_SIZE];
		u8 usb0_tx_buffer[USB_BUFFER_SIZE];
		// This is used as a TX buffer for CXC1101 FIFO, since the receive operation will also flush SPI TX buffer.
		u8 usb1_rx_buffer[USB_BUFFER_SIZE];
	} buffer;
	struct {
		u8 spi_buffer[USB_BUFFER_SIZE * 2];
		u8 usb0_buffer[USB_BUFFER_SIZE * 2];
		u8 usb1_buffer[USB_BUFFER_SIZE];
	} devbuffer;
	struct {
		u8 buffer[USB_BUFFER_SIZE * 5];
	} allbuffer;
}bufferUnion;
extern bufferUnion databuffer;

// *************************************************************************************************
#endif // __PROJECT_H
// End of file
