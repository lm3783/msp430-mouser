/***************************************************************
 *  SmartRF Studio(tm) Export
 *
 *  Radio register settings specifed with C-code
 *  compatible #define statements.
 *
 ***************************************************************/

// [BM] Modified radio settings for 433MHz, 868MHz, 915MHz

// ISM_LF configuration
//
// Chipcon
// Product = CC1101
// Chip version = A   (VERSION = 0x04)
// Crystal accuracy = 10 ppm
// X-tal frequency = 26 MHz
// RF output power = 0 dBm
// RX filterbandwidth = 232.142857 kHz
// Deviation = 32 kHz
// Datarate = 76.766968 kBaud
// Modulation = (1) GFSK
// Manchester enable = (0) Manchester disabled
// RF Frequency = 433.92 MHz
// Channel spacing = 199.951172 kHz
// Channel number = 0
// Optimization = -
// Sync mode = (3) 30/32 sync word bits detected
// Format of RX/TX data = (0) Normal mode, use FIFOs for RX and TX
// CRC operation = (1) CRC calculation in TX and CRC check in RX enabled
// Forward Error Correction = (0) FEC disabled
// Length configuration = (1) Variable length packets, packet length configured by the first
// received byte after sync word.
// Packetlength = 255
// Preamble count = (2)  4 bytes
// Append status = 1
// Address check = (0) No address check
// FIFO autoflush = 0
// Device address = 0
// GDO0 signal selection = ( 6) Asserts when sync word has been sent / received, and de-asserts at
// the end of the packet
// GDO2 signal selection = (41) CHIP_RDY

/***************************************************************
*  SmartRF Studio(tm) Export
*
*  Radio register settings specifed with C-code
*  compatible #define statements.
*
***************************************************************/

#ifndef SMARTRF_CC1101_H
#define SMARTRF_CC1101_H

#include "project.h"
#include "ccxx00.h"

typedef struct
{
   u8 addr;
   u8 data;
}registerSetting_t;

#define CC1101_PARAM_LENGTH 	62
#define CC1101_PATABLE_LENGTH	(8+1)

// Preamble count = 4
// Channel spacing = 199.951172
// Whitening = false
// Manchester enable = false
// CRC autoflush = false
// TX power = 0
// Data rate = 76.767
// Channel number = 0
// PA ramping = false
// Packet length mode = Variable packet length mode. Packet length configured by the first byte after sync word
// Deviation = 31.738281
// Modulation format = GFSK
// Address config = No address check
// CRC enable = true
// Modulated = true
// RX filter BW = 232.142857
// Sync word qualifier mode = 30/32 sync word bits detected
// Base frequency = 433.919830
// Device address = 0
// Data format = Normal mode
// Carrier frequency = 433.919830
// Packet length = 255
static const u8 patable[] = {CC1101_PATABLE, 0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,};

static const registerSetting_t preferredSettings[]=
{
  {CC1101_IOCFG2,      0x30},
  {CC1101_IOCFG0,      0x06},
  {CC1101_FIFOTHR,     0x47},
  {CC1101_PKTLEN,      0x40},
  {CC1101_PKTCTRL1,    0x04},
  {CC1101_PKTCTRL0,    0x05},
  {CC1101_FSCTRL1,     0x08},
  {CC1101_FREQ2,       0x10},
  {CC1101_FREQ1,       0xB0},
  {CC1101_FREQ0,       0x71},
  {CC1101_MDMCFG4,     0x7B},
  {CC1101_MDMCFG3,     0x83},
  {CC1101_MDMCFG2,     0x13},
  {CC1101_MDMCFG1,     0x22},
  {CC1101_DEVIATN,     0x42},
  {CC1101_MCSM0,       0x18},
  {CC1101_FOCCFG,      0x1D},
  {CC1101_BSCFG,       0x1C},
  {CC1101_AGCCTRL2,    0xC7},
  {CC1101_AGCCTRL1,    0x00},
  {CC1101_AGCCTRL0,    0xB2},
  {CC1101_WORCTRL,     0xFB},
  {CC1101_FREND1,      0xB6},
  {CC1101_FREND0,      0x10},
  {CC1101_FSCAL3,      0xEA},
  {CC1101_FSCAL2,      0x2A},
  {CC1101_FSCAL1,      0x00},
  {CC1101_FSCAL0,      0x1F},
  {CC1101_TEST2,       0x81},
  {CC1101_TEST1,       0x35},
  {CC1101_TEST0,       0x09},
};

// Manchester enable = false
// Modulation format = ASK/OOK
// PA ramping = false
// Data format = Asynchronous serial mode
// CRC autoflush = false
// Device address = 0
// Channel spacing = 199.951172
// Whitening = false
// Data rate = 2.39897
// Carrier frequency = 433.919830
// Sync word qualifier mode = No preamble/sync, carrier-sense above threshold
// TX power = 0
// CRC enable = false
// Packet length mode = Infinite packet length mode
// Preamble count = 2
// RX filter BW = 101.562500
// Deviation = 5.157471
// Channel number = 0
// Packet length = 13
// Address config = No address check
// Base frequency = 433.919830
// PA table
static const u8 ook_patable[] = {CC1101_PATABLE, 0x12,0x60,0x00,0x00,0x00,0x00,0x00,0x00,};

static const registerSetting_t preferredOOKSettings[]=
{
  {CC1101_IOCFG2,      0x30},
  {CC1101_IOCFG0,      0x0D},
  {CC1101_FIFOTHR,     0x47},
  {CC1101_PKTLEN,      0xFF},
  {CC1101_PKTCTRL1,    0x00},
  {CC1101_PKTCTRL0,    0x32},
  {CC1101_FSCTRL1,     0x06},
  {CC1101_FREQ2,       0x10},
  {CC1101_FREQ1,       0xB0},
  {CC1101_FREQ0,       0x71},
  {CC1101_MDMCFG4,     0x77},
  {CC1101_MDMCFG3,     0x83},
  {CC1101_MDMCFG2,     0x34},
  {CC1101_MDMCFG1,     0x02},
  {CC1101_DEVIATN,     0x15},
  {CC1101_MCSM0,       0x18},
  {CC1101_FOCCFG,      0x14},
  {CC1101_BSCFG,       0x6C},
  {CC1101_AGCCTRL2,    0xC4},
  {CC1101_AGCCTRL1,    0x20},
  {CC1101_AGCCTRL0,    0x92},
  {CC1101_WORCTRL,     0xFB},
  {CC1101_FREND1,      0x56},
  {CC1101_FREND0,      0x11},
  {CC1101_FSCAL3,      0xE9},
  {CC1101_FSCAL2,      0x2A},
  {CC1101_FSCAL1,      0x00},
  {CC1101_FSCAL0,      0x1F},
  {CC1101_TEST2,       0x81},
  {CC1101_TEST1,       0x35},
  {CC1101_TEST0,       0x09},
};

#endif
