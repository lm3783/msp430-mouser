// *************************************************************************************************
// Public header for all state definition.
// *************************************************************************************************

#ifndef STATEDEF_H_
#define STATEDEF_H_

// Radio state
#define RADIO_IDLE_PENDING	0	// Setting to idle mode and flush FIFO
#define RADIO_IDLE			1	// Idle state
#define RADIO_TX_PENDING	2	// TX request. DMA starts filling FIFO
#define RADIO_TX_FILLING	3	// DMA is filling FIFO
#define RADIO_TX_FILLED		4	// FIFO filled. Start transmitting
#define RADIO_TX_TRANSMITTING	5	// Transmitting
#define RADIO_TX_FINISHED	6	// Transmit complete
#define RADIO_RX_PENDING	7	// RX request. Start receiving
#define RADIO_RX_RECEIVING	8	// Receiving
#define RADIO_RX_FINISHED	9	// Receive complete. DMA starts filling buffer
#define RADIO_RX_FILLING	10	// DMA is filling buffer
#define RADIO_RX_FILLED		11	// Buffer filled. Checking CRC

#define RADIO_NOT_READY		0xFF	// Power on state

#define RADIO_TX_NOP		0	// stable state. no operation.
#define RADIO_TX_INIT		1	// TX pending. needs init.
#define RADIO_TX_EXIT		2	// TX finished. needs uninit.

// USB command
// *************************************************************************************************
// @fn          USB_COMM_NOP
// @brief       No operation. Test comm link.
// @param       none
// @return   1: always 0
// *************************************************************************************************
#define USB_COMM_NOP		0x00

// *************************************************************************************************
// @fn          USB_COMM_GET_VERSION
// @brief       Get software version
// @param       none
// @return   1: Version number
// *************************************************************************************************
#define USB_COMM_GET_VERSION		0x01

/* Operation mode:
 * 0: Uninitialized. Do nothing.
 * 1: Normal. Radio is in normal operation mode. Communicate with Chronos.
 * 2: remote control. Radio set to OOK mode.
  */

// *************************************************************************************************
// @fn          USB_COMM_GET_CURRENT_MODE
// @brief       Get current operation mode
// @param       none
// @return   1: mode(see above)
// *************************************************************************************************
#define USB_COMM_GET_CURRENT_MODE		0x02

// *************************************************************************************************
// @fn          USB_COMM_SET_CURRENT_MODE
// @brief       set current operation mode
// @param    1: mode(see above)
// @return   1: result(0:succuss, 1:fail)
// *************************************************************************************************
#define USB_COMM_SET_CURRENT_MODE		0x03

// *************************************************************************************************
// @fn          USB_COMM_CC1101_STROBE
// @brief       Directly send a strobe to CC1101
// @param    1: Strobe command.
//			 2: mode(0:read, 1:write)
// @return   1: SPI return value
// *************************************************************************************************
#define USB_COMM_CC1101_STROBE		0xF1

// *************************************************************************************************
// @fn          USB_COMM_CC1101_REGISTER
// @brief       Directly access an register of CC1101
// @param    1: register address
//           2: mode(0:read, 1:write)
//           3: Read: always 0; Write: register value
// @return   1: Read: register value; Write: always 0
// *************************************************************************************************
#define USB_COMM_CC1101_REGISTER		0xF2

// *************************************************************************************************
// @fn          USB_COMM_CC1101_IDLE
// @brief       Stop CC1101 operation, clear FIFO and set to idle.
// @param       none
// @return   1: Always 0
// *************************************************************************************************
#define USB_COMM_CC1101_IDLE		0xF3

// *************************************************************************************************
// @fn          USB_COMM_KEYBOARD
// @brief       Simulate a keyboard press event.
// @param    1: Keycode
// @return   1: Always 0
// *************************************************************************************************
#define USB_COMM_KEYBOARD		0xF4

// *************************************************************************************************
// @fn          USB_COMM_CC1101_TX
// @brief       Single TX mode. TX data was read from endpoint 1
// @param       none
// @return   1: Always 0
// *************************************************************************************************
#define USB_COMM_CC1101_TX		0xF5

// *************************************************************************************************
// @fn          USB_COMM_CC1101_RX
// @brief       Countinuous RX mode. RX data will be written to endpoint 1
// @param       none
// @return   1: Always 0
// *************************************************************************************************
#define USB_COMM_CC1101_RX		0xF6

// *************************************************************************************************
// @fn          USB_COMM_RF1101_CAPTURE
// @brief       Capture remote signal and send to endpoint 1. To exit capture, use USB_COMM_CC1101_IDLE
// @param       none
// @return   1: Always 0
// *************************************************************************************************
#define USB_COMM_RF1101_CAPTURE		0xF7


// Radio command
// *************************************************************************************************
// @fn          RADIO_OPERATION_NOP
// @brief       No operation. Test comm link.
// @param       none
// *************************************************************************************************
#define RADIO_OPERATION_NOP		0x00

// *************************************************************************************************
// @fn          RADIO_OPERATION_RSSI
// @brief       RSSI ping package.
// @param    1: package type. 0:single ping; 1:continuous ping(distance measure); 2:motion detection ping
//				3:ask ap to start continuous ping; 4:ask ap to stop continuous ping
// *************************************************************************************************
#define	RADIO_OPERATION_RSSI		0x01

// *************************************************************************************************
// @fn          RADIO_OPERATION_PPT
// @brief       keyboard simulation package.
// @param    1: keycode index. 0:reserved(reset); 1:up key; 2:down key; 3:single tap
// *************************************************************************************************
#define	RADIO_OPERATION_PPT		0x02

// *************************************************************************************************
// @fn          RADIO_OPERATION_BICOM
// @brief       bidirectional communication package.
// @param    1: type. 0:from watch to AP with ack; 1:from watch to AP with data; 2:from AP to watch with display text; 3:from AP to watch with data;
//		   2-x: ACK mode: serial number. Other mode: actual data.
// *************************************************************************************************
#define	RADIO_OPERATION_BICOM		0x03

#endif /* STATEDEF_H_ */
